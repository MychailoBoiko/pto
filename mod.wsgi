# PTO_project WSGI script

import sys
import os
import django
sys.path.append('/var/www/pto_ts')
os.environ['DJANGO_SETTINGS_MODULE'] = 'PTO_project.settings'
os.environ['PYTHON_EGG_CACHE'] = '/var/www/pto_ts/.python-eggs'
django.setup()
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

var vacation_request = {
    init: function() {
        this.datePickerEvents();
        this.customSelectEvent();
    },
    datePickerEvents: function () {
        $(".js-from-date").datepicker({
            weekStart: 1,
            orientation: 'auto',
            autoclose: true,
            todayHighlight: true,
            startDate: $(".js-from-date").hasClass('date-picker') ? '+0d' : false
        })
            .on('changeDate', function(e) {
            var newDate = new Date(e.date);
            var parent = $(e.target).parents('.has-error');
            var endDateField = $('.date-picker.js-end-date');
            var endDateFieldUnlimited = $('.date-picker-unlimited.js-end-date');
            if (parent) {
                parent.removeClass('has-error');
                parent.find('.errorlist').remove();
            }

            var getEndDate = endDateField.datepicker('getDate');
            var endDate = getEndDate === null ? newDate : getEndDate < newDate ? newDate : getEndDate;

            var getEndDateUnlimited = endDateFieldUnlimited.datepicker('getDate');
            var endDateUnlimited = getEndDateUnlimited === null ? newDate : getEndDateUnlimited < newDate ? newDate : getEndDateUnlimited;

                endDateField
                .datepicker('setDate', endDate)
                .datepicker('destroy')
                .datepicker({
                    weekStart: 1,
                    orientation: 'auto',
                    autoclose: true,
                    todayHighlight: true,
                    startDate: $(".js-end-date").hasClass('date-picker') ? '+0d' : false
                });

                endDateFieldUnlimited
                    .datepicker('setDate', endDateUnlimited)
                    .datepicker('destroy')
                    .datepicker({
                        weekStart: 1,
                        orientation: 'auto',
                        autoclose: true,
                        todayHighlight: true,
                        startDate: $(".js-end-date").hasClass('date-picker') ? '+0d' : false
                    })
            ;
        });

        $(".js-end-date").datepicker({
            weekStart: 1,
            orientation: 'auto',
            autoclose: true,
            todayHighlight: true,
            startDate: $(".js-end-date").hasClass('date-picker') ? '+0d' : false,
        });

    },
    customSelectEvent: function() {
        var self = this;
        $('.js-select').selectric({
            maxHeight: 300
        });
        $('.js-select-type').selectric({
            arrowButtonMarkup: '<span class="button-circle dropdown-button "><i class="fa fa-chevron-down"></i></span>',
            maxHeight: 300,
            onChange: function(element) {
                if ($(element).val() === '14') {
                    $('.js-date-picker-container').each(function () {
                        $(this)
                            .removeClass('date-picker')
                            .addClass('date-picker-unlimited')
                            .datepicker('destroy')
                        ;
                    });
                } else {
                    $('.js-date-picker-container').each(function () {
                        $(this)
                            .removeClass('date-picker-unlimited')
                            .addClass('date-picker')
                            .datepicker('destroy')
                        ;
                    });
                }
                self.datePickerEvents();
            }
        }).on();
        $(document).on('click', '.js-btn-time', function() {
            $(this).closest('.js-col-time').find('.selectric').trigger('click');
        });
    }
};
$(document).ready(function () {
    vacation_request.init();
});



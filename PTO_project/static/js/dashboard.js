var dashboard;
dashboard = {
    init: function () {
        this.tabSwitch();
        this.fixedTableEvent();
        this.tooltipDayEvent();
        this.expandUserEvent();
        this.customScrollbarEvent();
        this.sendInviteEvent();
        this.deleteUserEvent();
        this.managersListEvents();
        this.employeesSearchEvents();
        this.calendarNavigation();
        this.teamVacationActions();
        this.vacationHoverEvent();
        this.employerCountEvent();
        this.historyVacationAction();
        this.setVacationForUser();
        this.modalLeavePopup();
        this.freezeHeader();
    },
    freezeHeader: function () {
        var mainHeader = $('.js-dashboard-main-header');
        if(mainHeader.length > 0) {
            var sticky = mainHeader.offset();
            var stickyTop = sticky.top;
            var welcomeTitle = $('.js-welcome-title');
            var contentHolder = $('.js-content-holder');
            $('body').on('DOMMouseScroll mousewheel scroll', function () {
                if(mainHeader) {
                    if (window.pageYOffset >= (stickyTop)) {
                        mainHeader.addClass("is-fixed");
                        contentHolder.addClass('is-freeze-header');
                        welcomeTitle.slideUp();
                    } else {
                        mainHeader.removeClass("is-fixed");
                        welcomeTitle.slideDown();
                        contentHolder.removeClass('is-freeze-header');
                    }
                }
            });
        }
    },
    freezeHeaderReset: function () {
        var mainHeader = $('.js-dashboard-main-header');
        var welcomeTitle = $('.js-welcome-title');
        var contentHolder = $('.js-content-holder');
        mainHeader.removeClass("is-fixed");
        welcomeTitle.slideDown();
        contentHolder.removeClass('is-freeze-header');
    },
    tabSwitch: function () {
        var self = this;
        $.address.init().bind('change', function (event) {
            $('.nav-view-item').removeClass('active');
            var path = event.path.replace(/\//gm, '');
            if (path.length > 0) {
                $('.nav-view-link[href="#' + path + '"]').closest('.nav-view-item').addClass('active');
            } else {
                $('.nav-view-link[href="#calendar"]').closest('.nav-view-item').addClass('active');
            }
            if (path == 'table' || path == 'history') {
                $('.js-nav-legend').hide();
            } else {
                //$('.js-nav-legend').show();
                $('.js-nav-legend').fadeIn(500);
            }
            if (path == 'table') {
                $('.js-export-block, .js-add-employee').fadeIn(500);
            } else {
                $('.js-export-block, .js-add-employee').hide();
            }
            $('#dashboard_tab_container').load(window.location.href, {view_type_id: path},
                function (response, status, xhr) {
                    if (xhr.status == 403) {
                        window.location.href = '/oauth2callback';
                    }
                    dashboard.fixedTableEvent();
                    dashboard.tooltipDayEvent();
                    dashboard.expandUserEvent();
                    dashboard.customScrollbarEvent();
                    dashboard.vacationHoverEvent();
                    dashboard.employerCountEvent();
                    dashboard.freezeHeaderReset();
                    //dashboard.modalLeavePopup();
                });
        });
    },
    fixedTableEvent: function () {
        var $table = $('.js-table-fixed');
        //var $fixedColumn = $table.clone().insertBefore($table).addClass('fixed-column');
        var $fixedColumn = $('.js-table-fixed-out').append($table.clone().addClass('fixed-column'));

        $fixedColumn.find('th:not(:first-child),td:not(:first-child)').remove();

        $fixedColumn.find('tr').each(function (i, elem) {
            $(this).height($table.find('tr:eq(' + i + ')').height());
        });
    },
    tooltipDayEvent: function () {
        var self = this;

        $('.js-day-tooltip').popover({
            placement: 'top',
            html: 'true',
            content: function () {
                return self._getPopoverContent($(this).parent().attr('data'));
            }
        });

        $('.js-last-day-tooltip').popover({
            placement: 'left',
            html: 'true',
            content: function () {
                return self._getPopoverContent($(this).parent().attr('data'));
            }
        }).closest('.popover-out').addClass('vertical');
        $('.js-first-day-tooltip').popover({
            placement: 'right',
            html: 'true',
            content: function () {
                return self._getPopoverContent($(this).parent().attr('data'));
            }
        }).closest('.popover-out').addClass('vertical');

        $('.js-popover-button').on('click', function (e) {
            e.stopPropagation();
        });
        $('.js-popover-button').on('show.bs.popover', function (e) {
            $('body').off('click.popov');
            e.stopImmediatePropagation();
            var $btn = $(this);
            $('.js-popover-button').each(function () {
                if (!$(this).is(e.target)) {
                    if ($(this).parents('.popover-out').find('.popover').length) {
                        $(this).trigger('click');
                    }
                }
            });
            $('body').on('click.popov', function (e) {
                if ($(e.target).hasClass('vacation-reason') || $(e.target).hasClass('btn-action--close') || $(e.target).hasClass('fa-close') || $('body').hasClass('modal-open')) return;

                $('.js-popover-button').each(function () {
                    if ($(this).parents('.popover-out').find('.popover').length) {
                        $(this).trigger('click');
                    }

                });
                $('body').off('click.popov');
            });
        }).on('hide.bs.popover', function () {
            //alert('hide');
            //$('.js-popover-overlay').remove();
            //console.log($(this));
        });
    },
    _getPopoverContent: function (vacationType) {
        var content = $('.js-content-action-last-day');
        content.find('#vacation_type').html(vacationType);
        return content.html();
    },
    expandUserEvent: function () {
        $('.js-link-expand').on('click', function (e) {
            e.preventDefault();
            var spoilers = $('.js-spoiler');
            if (!$(this).hasClass('expanded')) {
                $(this).addClass('expanded').text('Collapse all');
                $(spoilers).removeClass('not-active').addClass('active').slideDown();
            } else {
                $(this).removeClass('expanded').text('Expand all');
                $(spoilers).removeClass('active').addClass('not-active').slideUp();
            }
        });
        $('.js-spoiler-toggle').on('click', function () {
            var spoiler = $(this).closest('.js-calendar-user').find('.js-spoiler');
            if (!$(spoiler).hasClass('active')) {
                $(spoiler).removeClass('not-active').addClass('active').slideDown();
            } else {
                $(spoiler).removeClass('active').addClass('not-active').slideUp();
            }
            if ($('.not-active').length < 1) {
                $('.js-link-expand').addClass('expanded').text('Collapse all');
            } else {
                $('.js-link-expand').removeClass('expanded').text('Expand all');
            }
        });
    },
    customScrollbarEvent: function () {
        $(".js-custom-scroll").mCustomScrollbar({
            theme: "minimal-dark",
            // mouseWheelPixels:150,
            // scrollInertia:150,
            mouseWheelPixels:10,
            scrollInertia:50,
            mouseWheel:true,
            updateOnContentResize:true
        });
        $(".js-custom-scroll-x").mCustomScrollbar({
            theme: "dark-thin",
            axis: "x"
        });
    },
    sendInviteEvent: function () {
        $(document).on('click', '#send_invite', function () {
            var email = $('#invitation_email').val();
            $.post('/invitation/', {email: email}, function () {
                $('#addEmployee').modal('hide');
            });
        });
    },
    deleteUserEvent: function () {
        $(document).on('click', '.js-link-delete-user', function () {
            var user_id = parseInt($(this).attr('data'));
            $(document).on('click', '.js-button-delete-employee', function () {
                $.post('/user/' + user_id + '/', {
                    delete: 'true'
                }, function (result) {
                    if (result.status == 'ok') {
                        $('#dashboard_tab_container').load(window.location.href, {
                            view_type_id: 'table'
                        }, function () {
                            dashboard.customScrollbarEvent();
                        });
                        $('#deleteUser').modal('hide');
                        user_id = '';

                    } else {
                        alert(result.message);
                    }
                });
            });
        });
    },
    managersListEvents: function () {
        var self = this;
        $(document).on('click', '.js-manager_name', function () {
            var input = $('.js-managers_input');
            var manageId = $(this).data('id');
            input.val($(this).text());
            input.attr('data', $(this).attr('data'));
            input.attr('data-id', manageId);
            self.__updateUserList();
        });
    },
    employeesSearchEvents: function () {
        var self = this;
        $(document).on('keyup', '#employees_search', function () {
            self.__updateUserListSearch();
        });
    },
    __updateUserListSearch: function () {
        var searchText = $('#employees_search').val().trim();
        var usersTable = $('#users_table');
        usersTable.find('tbody tr').hide();
        var selector = '.user-name-container';
        if (searchText.length > 0) {
            searchText = searchText.replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
                return $1.toUpperCase();
            });
            selector += (":contains('" + searchText + "')");
        }
        usersTable.find(selector).parents('tr').show();
    },
    __updateUserList: function () {
        var manager_input = $('.js-managers_input');
        var manager_id = $(manager_input).attr('data');
        var usersTable = $('#users_table');
        var manageName = $(manager_input).val();
        var selector = 'tbody tr';
        var filterManagerId = $(manager_input).data('id');
        console.log('filterManagerId ' + filterManagerId);

        $('#dashboard_tab_container').load(window.location.href, {
                view_type_id: 'table',
                filter_by_manager_id: filterManagerId
            },
            function (response, status, xhr) {
                if (xhr.status == 403) {
                    window.location.href = '/oauth2callback';
                }
                console.log('success' + usersTable.length);
                console.log('manager_id ' + manager_id);
                $('#users_table').find('tbody tr').hide();
                $('.js-managers_input').attr('data', manager_id);
                $('.js-managers_input').attr('data-id', manager_id);
                $('.js-managers_input').val(manageName);
                if (manager_id != 'any') {
                    selector += ('.manager_' + manager_id);
                }
                if (manager_id != 'any') {
                    $('#users_table').find('.manager_' + manager_id).show();
                } else {
                    $('#users_table tr').show();
                }

                dashboard.fixedTableEvent();
                dashboard.expandUserEvent();
                dashboard.customScrollbarEvent();
            });
    },
    calendarNavigation: function () {
        $(document).on('click', '.js-calendar-nav.active', function () {
            var year = $(this).parent().attr('data_year');
            var month = $(this).parent().attr('data_month');
            var move = 1;
            if ($(this).hasClass('js-calendar-nav-prev')) {
                move = -1;
            }
            var path = window.location.hash.replace(/#/gm, '');
            $('#dashboard_tab_container').load(window.location.href, {
                view_type_id: path,
                year: year,
                month: month,
                move: move
            }, function () {
                dashboard.vacationHoverEvent();
                dashboard.teamViewSet();
                dashboard.employerCountEvent();
            });
        });
    },
    teamVacationActions: function () {
        var self = this;
        $(document).on('click', '#team_table .js-action-yes', function () {
            var vacation_status_id = 2;
            var vacation_id = parseInt($(this).parents('.js-calendar_day').attr('data'));
            $.post("/vacation_set_status/", {
                    vacation_status_id: vacation_status_id,
                    vacation_id: vacation_id
                },
                function (result) {
                    if (result.status == 'ok') {
                        self.reloadTeamContainer();
                    } else {
                        alert(result.message);
                    }
                });
        });
        $(document).on('click', '#team_table .js-action-delete', function () {
            var textReason;
            var vacation_status_id;
            var vacation_id = parseInt($(this).parents('.js-calendar_day').attr('data'));
            $(document).on('click', '.js-submit-reason', function () {
                var reasonValue = $('.js-field-reason').val();
                if (reasonValue.length > 4) {
                    textReason = reasonValue;
                    vacation_status_id = 4;

                    $('.js-field-reason').parent().removeClass('has-error');

                    $.post("/vacation_set_status/", {
                        vacation_status_id: vacation_status_id,
                        vacation_id: vacation_id,
                        manager_comment: textReason
                    }, function (result) {
                        if (result.status == 'ok') {
                            self.reloadTeamContainer();
                            $('#modalReason').modal('hide');
                        } else {
                            console.log(result.message);
                        }
                    });
                } else {
                    $('.js-field-reason').parent().addClass('has-error');
                }
            });
        });
    },

    reloadTeamContainer: function () {
        var calendarNavigation = $('#calendar_navigation');
        var year = calendarNavigation.attr('data_year');
        var month = calendarNavigation.attr('data_month');
        $('#dashboard_tab_container').load(window.location.href, {
            view_type_id: 'team',
            year: year,
            month: month
        }, function () {
            dashboard.tooltipDayEvent();
            dashboard.teamViewSet();
        })
    },

    historyVacationAction: function () {
        $(document).on('click', '#history_table .js-link-delete-vacation', function () {
            var vacation_id = parseInt($(this).attr('data'));
            $(document).off('click', '.js-button-delete-vacation').on('click', '.js-button-delete-vacation', function () {
                $.post("/vacation_set_status/", {
                        vacation_id: vacation_id,
                        vacation_del: '1'
                    },
                    function (result) {
                        if (result.status == 'ok') {
                            $('#dashboard_tab_container').load(window.location.href, {
                                view_type_id: 'history'
                            });
                            $('#deleteVacation').modal('hide');
                            $('body').removeClass('modal-open');
                            $('.modal-backdrop').remove();
                            vacation_id = '';
                        } else {
                            alert(result.message);
                        }
                    });
            });
        });
    },

    vacationHoverEvent: function () {
        $('.js-vacation-hover').hover(function () {
            var dataId = $(this).attr('data-id');
            $('.js-vacation-hover[data-id=' + dataId + ']').addClass('hover');
        }, function () {
            var dataId = $(this).attr('data-id');
            $('.js-vacation-hover[data-id=' + dataId + ']').removeClass('hover');
        });
    },

    teamViewSet: function () {
        dashboard.fixedTableEvent();
        dashboard.tooltipDayEvent();
        dashboard.customScrollbarEvent();
    },

    countRow: function (weekElem) {
        var maxCountEmployee = 0;
        var days = $(weekElem).find('.js-calendar-day');
        for (var d = 0; d < days.length; d++) {
            var day = days[d];
            var res = $(day).find('.js-employee').length;
            if (res > maxCountEmployee) {
                maxCountEmployee = res;
                //console.log('maxCountEmployee changed to: ' + maxCountEmployee, 'on day', d);
                if (maxCountEmployee > 6) {
                    $(days[d]).closest('.js-week-row').find('.js-calendar-day').css('height', (maxCountEmployee * 20 + 50) + 'px');
                }
            }

        }
        return maxCountEmployee;
    },

    employerCountEvent: function () {
        var maxCountEmployee = 0;
        var rowWeek = $('.js-week-row');
        var employee = $('.js-employee');
        for (var w = 0; w < rowWeek.length; w++) {
            var weekResult = this.countRow(rowWeek[w]);
        }
    },

    setVacationForUser: function () {
        $(document).on('click', '#btn_set_vacation', function () {
            $('#set_user_vacation_form').submit();


            /* var frmSetVacation = $('#set_user_vacation_form');
            $.ajax({
                type: "POST",
                url: frmSetVacation.attr('action'),
                data: frmSetVacation.serialize(),
                success: function (data) {
                    $('#set_vacation_container').html(data);
                },
                error: function (data, str) {
                    alert('Возникла ошибка: ' + data.responseCode);
                }
            }); */


            /* var frmSetVacation = $('#set_user_vacation_form');
            $.post(frmSetVacation.attr('action'), frmSetVacation.serialize(),
                function (result) {
                    if (result.status == 'ok') {
                        self.reloadTeamContainer();
                    } else {
                        alert(result.message);
                    }
                }); */

        });
    },

    modalLeavePopup: function () {
        $(document).on('click', '.js-button-leave', function (e) {
            var leaveDetails = $('.js-content-leave-details');
            var title = $(this).attr('data-popup-title');
            $('.js-content-leave-title').html(title);
            leaveDetails.empty();
            var jsonLeave = $(this).attr('data-vacations');
            var objLeave = JSON.parse(jsonLeave);

            for (var i = 0; i < objLeave.length; i++) {
                var labelForDays = (objLeave[i].days > 1) ? 'days' : 'day';
                var commentText = (objLeave[i].comment != '') ? ' (' + objLeave[i].comment + ')' : '';
                // var contentLeave = '<div>' + objLeave[i].days + ' ' + labelForDays + ' ' + objLeave[i].reason + commentText + '</div>';
                var vacationType = '';
                if (title == 'Other leave') {
                    vacationType = ' ' + objLeave[i].vacation_type;
                }
                var previous_year = '';
                if (objLeave[i].vacation_type == 'PTO previous year (January only)') {
                    previous_year = ' [previous year]'
                }
                var contentLeave = '<div>' + objLeave[i].start_date + ' - ' + objLeave[i].end_date + ' ' +
                    objLeave[i].days + ' ' + labelForDays + vacationType + commentText + previous_year + '</div>';
                leaveDetails.append(contentLeave);
            }
        });
    }
};
$(document).ready(function () {
    dashboard.init();
});

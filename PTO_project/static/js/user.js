var user = {
    init: function() {
        this.datePickerEvents();
        this.customSelectEvent();
        this.uploadPhotoEvent();
        this.removePhotoEvent();
    },
    datePickerEvents: function () {
        console.log('date-picker')
        $(".date-picker").datepicker({
            orientation: 'auto',
            autoclose: true,
            todayHighlight: true
            //startDate: '+1d'
        });
    },
    customSelectEvent: function() {
        $('.js-select').selectric({
            arrowButtonMarkup: '<span class="button-circle dropdown-button "><i class="fa fa-chevron-down"></i></span>',
            maxHeight: 300
        });
    },
    uploadPhotoEvent: function () {
        $(document).on('click', '.js-trigger-upload', function () {
            $(".js-native-upload").trigger("click");
        });
        $(".js-native-upload").on('change', function() {
            var fileUploadName = $(".js-native-upload").val();
            if(fileUploadName.length > 0 ) {
                var fileUploadNameSplit = fileUploadName.split(/(\\|\/)/g).pop();
                $('.js-text-upload').text(fileUploadNameSplit);
            } else {
                $('.js-text-upload').text('No file chosen');
            }
        });
    },

    removePhotoEvent: function () {
        $('.js-trigger-remove-photo').on('click', function () {
            var userId = $('#current_user_id').val();
            $.ajax({
                type: "POST",
                url: '/user/' + userId + '/',
                data: {'remove_photo': '1'},
                success: function (data) {
                    $('#avatar_container').html('<i class="fa fa-user"></i>');
                },
                error: function (data, str) {
                    alert('Error: ' + data.responseCode);
                }
            });
        });

    }
};
$(document).ready(function () {
    user.init();
});



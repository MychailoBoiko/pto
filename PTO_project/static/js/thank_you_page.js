var thank_you = {
    init: function() {
        this.redirectToHomePage(5);
    },
    redirectToHomePage: function (seconds) {
        setInterval(function () {
            seconds--;
            if (seconds == 0) {
                window.location.href = "/";
            }
            var text = seconds + ' second';
            if (seconds > 1)
                text += 's';
            $("#js-timer_down").html(text);
        }, 1000);
    }
};
$(document).ready(function () {
    thank_you.init();
});

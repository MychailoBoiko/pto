# -*- coding: utf-8 -*-
"""PTO_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from PTO_project_core.views import test, VacationRequest, Dashboard, VacationSetStatus, Invitation, User, \
    SetUserVacation, oauth2callback, logout, export_user_vacations, ManagerSetUserVacation

admin.site.site_header = 'PTO administration'

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    # url(r'^login/$', LoginToPTO.as_view(), name='login'),

    url(r'^vacation_request/$', VacationRequest.as_view(), name='vacation_request'),
    url(r'^dashboard/$', Dashboard.as_view(), name='dashboard'),
    url(r'^vacation_set_status/$', VacationSetStatus.as_view(), name='vacation_set_status'),
    url(r'^user/(\d+)/$', User.as_view(), name='user'),

    url(r'^set_user_vacation/(\d+)/$', SetUserVacation.as_view(), name='set_user_vacation'),

    url(r'^manager_set_user_vacation/(\d+)/$', ManagerSetUserVacation.as_view(), name='manager_set_user_vacation'),

    url(r'^export_user_vacations/$', export_user_vacations, name='export_user_vacations'),

    url(r'^invitation/$', Invitation.as_view(), name='invitation'),

    url(r'^test/$', test),

    url(r'^$', Dashboard.as_view(), name='dashboard'),

    url(r'^oauth2callback/$', oauth2callback, name='oauth2callback'),
    url(r'^logout/$', logout, name='logout'),



] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
  + static('/.well-known/', document_root='/var/www/html/.well-known/')

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]

# -*- coding: utf-8 -*-
import datetime
from django.shortcuts import redirect, render
from django.http import HttpResponse
from functools import wraps
import json
import os
from PTO_project_core.models import PTOUser, PTOCompany
from oauth2client import client, crypt
import httplib2
from apiclient import discovery


PTO_GOOGLE_CLIENT_ID = '1015088249229-ji0tnduit3shtppojf3b012j53a5an1b.apps.googleusercontent.com'
CLIENT_SECRET = os.path.join(
    os.path.dirname(__file__), '..',
    'client_secret_1015088249229-ji0tnduit3shtppojf3b012j53a5an1b.apps.googleusercontent.com.json'
)


def get_flow(base_url):
    flow = client.flow_from_clientsecrets(
        filename=CLIENT_SECRET,
        scope='https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email',
        redirect_uri='http://{}/oauth2callback'.format(base_url)
    )
    # flow.params['access_type'] = 'offline'
    flow.params['include_granted_scopes'] = 'true'
    # flow.params['approval_prompt'] = 'auto'
    # flow.params['approval_prompt'] = 'force'
    return flow


def pto_auth_required(view_func):
    @wraps(view_func)
    def decorated(request, *args, **kwargs):
        if 'pto_google_auth' not in request.session or not PTOUserSession.token_verified(
                request.session['pto_google_auth']['credentials']['token_response']['id_token']):
            if not request.is_ajax():
                return redirect('oauth2callback')
            else:
                return HttpResponse('Authorization required', status=403)

        try:
            if PTOUser.objects.get(id=request.session['pto_google_auth']['PTOUser']['id']).deleted:
                return render(request, 'login_errors.html', {
                    'login_errors': 'Your account was deleted, please contact admin'
                })
        except PTOUser.DoesNotExist:  # User mysteriously disappeared, during session. Don't ask how.
            del request.session['pto_google_auth']
            return redirect('/')

        return view_func(request, *args, **kwargs)
    return decorated


# def token_auto_refresh(view_func):
#     @wraps(view_func)
#     def decorated(request, *args, **kwargs):
#
#         if 'pto_google_auth' in request.session and 'credentials' in request.session['pto_google_auth']:
#             credentials = client.OAuth2Credentials.from_json(
#                 json.dumps(request.session['pto_google_auth']['credentials']))
#
#             try:
#                 if credentials.access_token_expired:
#                     credentials.refresh(httplib2.Http())
#                     request.session['pto_google_auth']['credentials'] = json.loads(credentials.to_json())
#
#             except client.HttpAccessTokenRefreshError:
#                 return redirect('oauth2callback')
#
#         return view_func(request, *args, **kwargs)
#     return decorated


class PTOUserSession(object):
    @staticmethod
    def get(request):
        return request.session['pto_google_auth']

    def set(self, request, _credentials):
        credentials = _credentials
        if credentials.access_token_expired:
            try:
                credentials.refresh(httplib2.Http())
                request.session['pto_google_auth']['credentials'] = json.loads(credentials.to_json())
            except:
                return redirect('oauth2callback')
        else:
            http_auth = credentials.authorize(httplib2.Http())
            users_service = discovery.build('oauth2', 'v2', http=http_auth)
            user_data = users_service.userinfo().get().execute()
            pto_user_dict = self.__get_or_create_user(user_data)
            if pto_user_dict is not None:
                request.session['pto_google_auth'] = {
                    'PTOUser': pto_user_dict,
                    'credentials': json.loads(credentials.to_json())
                }
                request.session.set_expiry(86400)  # 24 hours
                # request.session.set_expiry(86400 * 365 * 10)  # 10 years
                return {}
            else:
                return {'message': 'User or company is not registered!'}

    @staticmethod
    def __get_or_create_user(user_data):
        try:
            pto_user_dict = PTOUser.objects.values().get(email=user_data['email'])
        except PTOUser.DoesNotExist:
            try:
                name, domain = user_data['email'].split('@')
                company = PTOCompany.objects.get(domain=domain)
                now = datetime.datetime.now()
                pto_user = PTOUser(
                    email=user_data['email'],
                    first_name=user_data['given_name'],
                    last_name=user_data['family_name'],
                    pto_company=company,
                    employment_start_datetime=datetime.datetime(year=now.year,month=now.month,day=now.day)
                )
                pto_user.save()
                pto_user_dict = PTOUser.objects.values().get(email=pto_user.email)

            except PTOCompany.DoesNotExist:
                return None

        pto_user_dict['create_datetime'] = pto_user_dict['create_datetime'].strftime(
            "%Y-%m-%d %H:%M:%S") if 'create_datetime' in pto_user_dict.keys() else ''

        if pto_user_dict['employment_start_datetime'] is not None:
            pto_user_dict['employment_start_datetime'] = pto_user_dict['employment_start_datetime'].strftime(
                "%Y-%m-%d %H:%M:%S")

        pto_user_dict['photo'] = pto_user_dict['photo'] if pto_user_dict['photo'] else ''

        return pto_user_dict

    @staticmethod
    def token_verified(token):
        try:
            client.verify_id_token(token, PTO_GOOGLE_CLIENT_ID)
            return True
        except crypt.AppIdentityError:
            return False

# -*- coding: utf-8 -*-
from django import forms

from PTO_project_core.helpers import CalendarBuilder as CalendarBuilderFile
from PTO_project_core.helpers.PtoVacationValidationHelper import PtoVacationValidationHelper
from PTO_project_core.models import PTOUser, PTORole, PTOUserStatus, VacationType


class VacationForm(forms.Form):
    start_date = forms.DateTimeField(widget=forms.DateInput(
        attrs={'class': 'form-control form-control--tall js-start-date', 'autocomplete': 'off'}))
    end_date = forms.DateTimeField(widget=forms.DateInput(
        attrs={'class': 'form-control form-control--tall', 'autocomplete': 'off'}))
    start_time = forms.FloatField(widget=forms.HiddenInput(), initial=CalendarBuilderFile.START_WORKING_HOUR)
    end_time = forms.FloatField(widget=forms.HiddenInput(), initial=CalendarBuilderFile.END_WORKING_HOUR)
    vacation_type = forms.IntegerField(required=True, widget=forms.Select(
        # comment VacationType here, if "migrate" calls at first time =/
        choices=VacationType.get_vacation_type_choices(),
        attrs={'class': 'form-control select-vacation form-control--tall js-select-type'}
    ))
    comment = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control form-control--tall', 'placeholder': 'your current projects'}
    ))
    pto_user_id = forms.IntegerField(widget=forms.HiddenInput())

    def is_valid(self):
        valid = super(VacationForm, self).is_valid()
        if not valid:
            return valid

        pto_user_id_ = self.cleaned_data['pto_user_id']
        pto_user_ = PTOUser.objects.get(id=pto_user_id_)
        v_type = VacationType.objects.get(id=self.cleaned_data['vacation_type'])

        pto_vacation_validation_helper = PtoVacationValidationHelper(form=self, user=pto_user_,
                                                                     start_date=self.cleaned_data['start_date'],
                                                                     end_date=self.cleaned_data['end_date'])

        return \
            pto_vacation_validation_helper.base_validate and \
            pto_vacation_validation_helper.not_paid_validate(v_type) and \
            pto_vacation_validation_helper.work_from_home_validate(v_type) and \
            pto_vacation_validation_helper.pto_validate(v_type) and \
            pto_vacation_validation_helper.previous_year_pto_validate(v_type) and \
            pto_vacation_validation_helper.intersection_validate()


class PTOUserForm(forms.Form):
    photo = forms.ImageField(widget=forms.FileInput(attrs={'class': 'js-native-upload native-upload'}), required=False)
    position = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control--tall'}))
    employment_start_datetime = forms.DateField(widget=forms.DateInput(
        attrs={'class': 'form-control form-control--tall', 'autocomplete': 'off'}
    ))
    pto_user_manager = forms.IntegerField(required=False, widget=forms.Select(
        attrs={'class': 'form-control form-control--tall'}
    ))
    pto_role = forms.IntegerField(required=False, widget=forms.Select(
        choices=PTORole.objects.all().values_list('id', 'name'),
        attrs={'class': 'form-control form-control--tall js-select'}
    ))
    pto_user_status = forms.IntegerField(required=False, widget=forms.Select(
        choices=PTOUserStatus.objects.all().values_list('id', 'name'),
        attrs={'class': 'form-control form-control--tall js-select'}
    ))


class SetUserVacationForm(forms.Form):
    start_date = forms.DateTimeField(required=True, widget=forms.DateInput(
        attrs={'class': 'form-control form-control--tall js-start-date', 'autocomplete': 'off', 'required': 'true'}))
    end_date = forms.DateTimeField(required=True, widget=forms.DateInput(
        attrs={'class': 'form-control form-control--tall', 'autocomplete': 'off', 'required': 'true'}))
    vacation_type = forms.IntegerField(required=True, widget=forms.Select(
        # comment VacationType here, if "migrate" calls at first time =/
        choices=VacationType.objects.values_list('id', 'name'),
        attrs={'class': 'form-control select-vacation form-control--tall js-select-type'}
    ))
    pto_user_id = forms.IntegerField(widget=forms.HiddenInput())
    comment = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control form-control--tall'}
    ))

    def is_valid(self):
        valid = super(SetUserVacationForm, self).is_valid()

        if not valid:
            return valid

        v_type = VacationType.objects.get(id=self.cleaned_data['vacation_type'])
        pto_user_id_ = self.cleaned_data['pto_user_id']
        pto_user_ = PTOUser.objects.get(id=pto_user_id_)

        pto_vacation_validation_helper = PtoVacationValidationHelper(form=self, user=pto_user_,
                                                                     start_date=self.cleaned_data['start_date'],
                                                                     end_date=self.cleaned_data['end_date'])

        return \
            pto_vacation_validation_helper.base_validate and \
            pto_vacation_validation_helper.intersection_validate()


class ManagerSetUserVacationForm(forms.Form):
    start_date = forms.DateTimeField(required=True, widget=forms.DateInput(
        attrs={'class': 'form-control form-control--tall js-start-date', 'autocomplete': 'off', 'required': 'true'}))
    end_date = forms.DateTimeField(required=True, widget=forms.DateInput(
        attrs={'class': 'form-control form-control--tall', 'autocomplete': 'off', 'required': 'true'}))
    vacation_type = forms.IntegerField(required=True, widget=forms.Select(
        choices=VacationType.objects.filter(can_be_used_by_manager=True).values_list('id', 'name'),
        attrs={'class': 'form-control select-vacation form-control--tall js-select-type'}
    ))
    pto_user_id = forms.IntegerField(widget=forms.HiddenInput())
    comment = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control form-control--tall'}
    ))

    def is_valid(self):
        valid = super(ManagerSetUserVacationForm, self).is_valid()

        if not valid:
            return valid

        pto_user_id_ = self.cleaned_data['pto_user_id']
        pto_user_ = PTOUser.objects.get(id=pto_user_id_)
        v_type = VacationType.objects.get(id=self.cleaned_data['vacation_type'])

        pto_vacation_validation_helper = PtoVacationValidationHelper(form=self, user=pto_user_,
                                                                     start_date=self.cleaned_data['start_date'],
                                                                     end_date=self.cleaned_data['end_date'])

        return \
            pto_vacation_validation_helper.base_validate and \
            pto_vacation_validation_helper.intersection_validate() and \
            pto_vacation_validation_helper.force_majeure_validate(v_type)

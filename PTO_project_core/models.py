# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
from django.db import models

DAYS_IN_YEAR = 365
DAYS_IN_HALF_YEAR = 183
DAYS_FIREPROOF_PERIOD = 548
CATEGORY_1 = 0.0411
CATEGORY_2 = 0.0548
CATEGORY_3 = 0.0685
CATEGORY_4 = 0.07672


class PTORole(models.Model):
    name = models.CharField(max_length=31)
    create_datetime = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        db_table = 'pto_role'
        managed = True

    def __unicode__(self):
        return self.name


class PTOCompany(models.Model):
    name = models.CharField(max_length=255)
    domain = models.CharField(max_length=255, blank=True)
    create_datetime = models.DateTimeField(auto_now_add=True, blank=True)
    active = models.BooleanField(default=True)

    class Meta:
        db_table = 'pto_company'
        verbose_name_plural = 'Pto companies'
        managed = True

    def __unicode__(self):
        return self.name


class PTOUserStatus(models.Model):
    name = models.CharField(max_length=31)
    create_datetime = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        db_table = 'pto_user_status'
        managed = True

    def __unicode__(self):
        return self.name


class PTOUser(models.Model):
    first_name = models.CharField(max_length=63)
    middle_name = models.CharField(max_length=63, null=True, blank=True)
    last_name = models.CharField(max_length=63)
    email = models.EmailField()
    photo = models.ImageField(upload_to='user_avatars', null=True, blank=True)
    position = models.CharField(max_length=63)
    create_datetime = models.DateTimeField(auto_now_add=True, blank=True)
    employment_start_datetime = models.DateTimeField(null=True, blank=True)
    pto_role = models.ForeignKey(PTORole, verbose_name='Role', default=2)
    pto_user_status = models.ForeignKey(PTOUserStatus, verbose_name='Status', null=True, blank=True)
    pto_company = models.ForeignKey(PTOCompany, verbose_name='Company', null=True, blank=True)
    pto_user_manager = models.ForeignKey('self', verbose_name='Manager', null=True, blank=True)
    refresh_token = models.CharField(null=True, blank=True, max_length=255)
    deleted = models.BooleanField(default=False)

    def get_available_vacation_days(self):
        return self.__get_available_vacation_days(datetime.datetime.now())

    def get_current_start_of_period_date(self):
        return self.get_start_of_period_date(datetime.datetime.now())

    def get_current_end_of_period_date(self):
        return self.get_end_of_period_date(datetime.datetime.now())

    def get_current_period_available_vacation_days(self):
        return self.__get_available_vacation_days(self.get_current_end_of_period_date())

    def get_period_available_vacation_days_for_date(self, date):
        return self.__get_available_vacation_days(date)

    def get_vacations_sum(self, date):
        from PTO_project_core.helpers.UsersHelper import PAID_VACATION_NAME
        return self.get_vacations_sum_for_type(date, PAID_VACATION_NAME)

    def get_next_year_vacations_sum(self, date):
        from PTO_project_core.helpers.UsersHelper import PTO_PREVIOUS_YEAR
        return self.get_vacations_sum_for_type(date, PTO_PREVIOUS_YEAR)

    def get_vacations_sum_for_type(self, date, vacation_type_name):
        start_date = self.get_start_of_period_date(date)
        end_date = self.get_end_of_period_date(date)
        statuses = ['new', 'scheduled']
        vacations = Vacation.objects.filter(
            pto_user__id=self.id, status__text__in=statuses, vacation_type__name=vacation_type_name,
            start_date__lte=end_date, end_date__gte=start_date
        )
        days_sum = 0
        for vacation in vacations:
            current_start_date = max(start_date, vacation.start_date)
            current_end_date = min(end_date, vacation.end_date)
            from PTO_project_core.helpers.CalendarBuilder import CalendarBuilder
            days_sum += CalendarBuilder.count_working_day_for_range(current_start_date, current_end_date).days

        return days_sum

    def is_first_year(self, date):
        return self.employment_start_datetime.year == date.year

    def is_second_year(self, date):
        return self.employment_start_datetime.year == date.year - 1

    def is_third_year(self, date):
        return self.employment_start_datetime.year == date.year - 2

    def is_long_term(self, date):
        return self.employment_start_datetime.year < date.year - 1

    def get_start_of_period_date(self, date):
        if self.is_first_year(date):
            return self.employment_start_datetime
        else:
            return datetime.datetime(year=date.year, month=1, day=1)

    @staticmethod
    def get_end_of_period_date(date):
        return datetime.datetime(year=date.year, month=12, day=31, hour=23)

    def get_end_of_next_period_date(self, date):
        next_year_date = self.get_end_of_period_date(date) + datetime.timedelta(days=1)
        return self.get_end_of_period_date(next_year_date)

    def get_half_year_text(self):
        current_date = datetime.datetime.now()
        half_year_date = self.employment_start_datetime + datetime.timedelta(days=DAYS_IN_HALF_YEAR)
        if half_year_date > current_date:
            return 'You can take vacation from <strong>' + half_year_date.strftime('%m/%d/%Y') + '</strong>.<br>'
        return ''

    def __get_available_vacation_days(self, date):
        date_interval = date - self.employment_start_datetime
        current_days = date_interval.days
        if current_days < DAYS_IN_HALF_YEAR:
            return -1
        if self.is_first_year(date):
            return int(round(current_days * CATEGORY_1))
        if self.is_second_year(date):
            if current_days > DAYS_IN_YEAR:
                days = int(round((current_days - DAYS_IN_YEAR) * CATEGORY_2 + DAYS_IN_YEAR * CATEGORY_1))
            else:
                days = int(round(current_days * CATEGORY_1))
            previous_period_date = self.get_current_start_of_period_date() - datetime.timedelta(days=1)
            previous_period_taken = self.get_vacations_sum(previous_period_date)
            return days - previous_period_taken
        else:
            from_date = self.get_start_of_period_date(date)
            to_date = self.get_end_of_period_date(date)
            if date < to_date:
                to_date = date
            from_index = PTOUser.__get_index_for_date_interval(from_date - self.employment_start_datetime)
            to_index = PTOUser.__get_index_for_date_interval(to_date - self.employment_start_datetime)
            if from_index == to_index:
                interval = to_date - from_date
                return int(round(interval.days * from_index))
            else:
                middle = datetime.datetime(year=date.year,
                                           month=self.employment_start_datetime.month,
                                           day=self.employment_start_datetime.day)
                from_interval = middle - from_date
                to_interval = to_date - middle
                return int(round(from_interval.days * from_index + to_interval.days * to_index))

    @staticmethod
    def __get_index_for_date_interval(date_interval):
        index = CATEGORY_1
        if date_interval.days > DAYS_IN_YEAR:
            index = CATEGORY_2
        if date_interval.days > 3 * DAYS_IN_YEAR:
            index = CATEGORY_3
        if date_interval.days > 5 * DAYS_IN_YEAR:
            index = CATEGORY_4
        return index

    class Meta:
        db_table = 'pto_user'
        managed = True

    def __unicode__(self):
        return '{} {}'.format(self.first_name, self.last_name)


class VacationType(models.Model):
    name = models.CharField(max_length=63)
    use_in_calculations = models.BooleanField(default=True)
    can_be_used_by_employee = models.BooleanField(default=True)
    can_be_used_by_manager = models.BooleanField(default=True)
    create_datetime = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        db_table = 'vacation_type'
        managed = True

    def __unicode__(self):
        return self.name

    @staticmethod
    def get_vacation_type_choices():
        return VacationType.objects.filter(can_be_used_by_employee=True).values_list('id', 'name')


class VacationStatus(models.Model):
    text = models.CharField(max_length=31)
    display_name = models.CharField(max_length=255, null=True)
    create_datetime = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        managed = True
        db_table = 'vacation_status'

    def __unicode__(self):
        return self.text


class Vacation(models.Model):
    vacation_type = models.ForeignKey(VacationType, verbose_name='Vacation type', null=True, blank=True)
    comment = models.CharField(max_length=255, null=False, blank=False)
    explanation = models.CharField(max_length=255, null=True, blank=True)
    pto_user = models.ForeignKey(PTOUser, verbose_name='Vacation user')
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    create_datetime = models.DateTimeField(auto_now_add=True, blank=True)
    status = models.ForeignKey(VacationStatus, verbose_name='Vacation status')
    manager_reason_comment = models.CharField(max_length=255, verbose_name='Manager`s comment about reason', null=True)

    class Meta:
        managed = True
        db_table = 'vacation'


class Holiday(models.Model):
    holiday_name = models.CharField(max_length=255)
    holiday_date = models.DateField()
    is_working_day = models.BooleanField(default=False)
    create_datetime = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        managed = True
        db_table = 'holiday'

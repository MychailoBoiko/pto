# -*- coding: utf-8 -*-

from django.contrib import admin
from PTO_project_core.models import VacationType, PTOCompany, PTOUser, PTORole, PTOUserStatus, Holiday


class VacationTypeAdmin(admin.ModelAdmin):
    list_display = ['name', 'use_in_calculations']
    search_fields = ['name']


class PTOCompanyAdmin(admin.ModelAdmin):
    list_display = ['name', 'create_datetime']
    search_fields = ['name']


class PTOUserAdmin(admin.ModelAdmin):
    list_display = ['last_name', 'first_name', 'email', 'position', 'create_datetime']
    search_fields = ['last_name', 'first_name', 'email']


class PTORoleAdmin(admin.ModelAdmin):
    list_display = ['name', 'create_datetime']
    search_fields = ['name']


class PTORoleStatus(admin.ModelAdmin):
    list_display = ['name', 'create_datetime']
    search_fields = ['name']


class HolidayAdmin(admin.ModelAdmin):
    list_display = ['holiday_name', 'holiday_date']
    search_fields = ['holiday_name']


admin.site.register(Holiday, HolidayAdmin)
admin.site.register(PTORole, PTORoleAdmin)
admin.site.register(PTOUserStatus, PTORoleStatus)
admin.site.register(PTOUser, PTOUserAdmin)
admin.site.register(PTOCompany, PTOCompanyAdmin)
admin.site.register(VacationType, VacationTypeAdmin)

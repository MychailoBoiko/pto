# -*- coding: utf-8 -*-
import datetime
import json
import django_excel as excel
from pyexcel_xlsx import write_data  # do not delete this import needed for excel export
from oauth2client import client
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.utils import formats
from django.http import HttpResponse, JsonResponse, HttpResponseForbidden
from django.utils.decorators import method_decorator
from django.views.generic import View

from PTOAuthentication.PTOAuth import pto_auth_required, PTOUserSession, get_flow  # , token_auto_refresh
from PTO_project_core.forms import VacationForm, PTOUserForm, SetUserVacationForm, ManagerSetUserVacationForm
from PTO_project_core.helpers.PtoVacationValidationHelper import MAX_PTO_PREVIOUS_YEAR_DAYS
from PTO_project_core.helpers.UsersHelper import UsersHelper, PTO_PREVIOUS_YEAR
from PTO_project_core.helpers.CalendarBuilder import CalendarBuilder, DateHour
from PTO_project_core.helpers.PTOAsyncMailer import send_mail_async, vacation_notify, manager_notify
# from PTO_project_core.helpers.TemplateOperation import TemplateOperation
from PTO_project_core.helpers.UserPermissions import UserPermissions, ROLE_HR_MANAGER, ROLE_MANAGER, ROLE_USER
from PTO_project_core.models import VacationType, Vacation, PTOUser, PTOUserStatus, VacationStatus, PTORole
from PTO_project_core.vacations_data_builder.VacationDataBuilder import VacationDataBuilder
from PTO_project.settings import DEBUG

user_session = PTOUserSession()


def test(request):
    if request.method == 'POST' and request.is_ajax() and 'del_pto_session' in request.POST:

        if 'pto_google_auth' in request.session.keys():
            del request.session['pto_google_auth']

        return HttpResponse('PTO session deleted.')

    return render(request, 'test.html')


# @method_decorator(token_auto_refresh, name='dispatch')
@method_decorator(pto_auth_required, name='dispatch')
class Dashboard(View):
    @staticmethod
    def get(request):
        return render(request, 'dashboard/dashboard.html', {
            'is_manager': UserPermissions(request).role_id != ROLE_USER,
            'is_hr_manager': UserPermissions(request).role_id == ROLE_HR_MANAGER
        })

    def post(self, request):
        if request.is_ajax():
            return self.__render_tab(request, request.POST['view_type_id'])

    def __render_tab(self, request, view_type_id='calendar'):
        if view_type_id == 'calendar' or view_type_id == '':
            return self.__render_calendar_tab(request)

        elif view_type_id == 'table':
            if UserPermissions(request).role_id != ROLE_USER:
                return self.__render_table_tab(request)
        elif view_type_id == 'team':
            if UserPermissions(request).role_id != ROLE_USER:
                return self.__render_team_tab(request)

        elif view_type_id == 'history':
            return self.__render_history_tab(request)
        else:
            raise Exception('Not valid view_type_id!')

    def __render_calendar_tab(self, request):
        year, month = self.__get_year_month_from_post(request)
        calendar = CalendarBuilder(year, month)
        vacation_data_builder = VacationDataBuilder(request, year, month)
        vacation_per_calendar = vacation_data_builder.get_data_for_calendar()

        return render(request, 'dashboard/_calendar.html', {
            'calendar': calendar,
            'vacation_per_calendar': vacation_per_calendar,
        })

    @staticmethod
    def __render_table_tab(request):
        if UserPermissions(request).role_id == ROLE_HR_MANAGER:
            users = PTOUser.objects.filter(
                # pto_company__id=request.session['pto_google_auth']['PTOUser']['pto_company_id'],
                deleted=False
            )
            if 'filter_by_manager_id' in request.POST and request.POST['filter_by_manager_id'] != 'any':
                if request.POST['filter_by_manager_id'] == 'none':
                    users = users.filter(pto_user_manager__id__isnull=True)
                else:
                    users = users.filter(pto_user_manager__id=request.POST['filter_by_manager_id'])
        else:
            users = PTOUser.objects.filter(
                # pto_company__id=request.session['pto_google_auth']['PTOUser']['pto_company_id'],
                pto_user_manager__id=request.session['pto_google_auth']['PTOUser']['id'],
                deleted=False
            )
        totals = UsersHelper().calculate_days(users)
        managers_ids = PTOUser.objects.filter(deleted=False).exclude(
            pto_user_manager__isnull=True).values('pto_user_manager')
        managers = PTOUser.objects.filter(id__in=managers_ids)
        return render(request, 'dashboard/_table.html', {
            'users': users,
            'totals': totals,
            'managers': managers,
        })

    def __render_team_tab(self, request):
        year, month = self.__get_year_month_from_post(request)
        calendar = CalendarBuilder(year, month)
        vacation_data_builder = VacationDataBuilder(request, year, month)
        vacations_for_table = vacation_data_builder.get_data_for_calendar_table()
        has_prev, has_next = vacation_data_builder.get_next_prev_availability()
        return render(request, 'dashboard/_team.html', {
            'calendar': calendar,
            'vacations_for_table': vacations_for_table,
            'has_prev': has_prev,
            'has_next': has_next,
        })

    def __render_history_tab(self, request):
        pto_user_id = request.session['pto_google_auth']['PTOUser']['id']
        current_pto_user = PTOUser.objects.get(id=pto_user_id)
        start_of_year = datetime.datetime(year=datetime.datetime.now().year, month=1, day=1, hour=1)
        vacations = Vacation.objects.filter(
            pto_user=current_pto_user,
            end_date__gte=start_of_year
        ).order_by('start_date')
        total_days_hours = DateHour()
        for vacation in vacations:
            working_day_for_range = CalendarBuilder.count_working_day_for_range(vacation.start_date, vacation.end_date)
            vacation.working_days = working_day_for_range
            total_days_hours.add_days_hours(working_day_for_range)
            enable_delete = False
            if vacation.status.text == 'new':
                enable_delete = True
            elif vacation.status.text == 'scheduled' and vacation.start_date >= self.today_start():
                enable_delete = True
            vacation.enable_delete = enable_delete
        return render(request, 'dashboard/_history.html', {
            'vacations': vacations,
            'total_days_hours': total_days_hours
        })

    @staticmethod
    def today_start():
        return datetime.datetime(
            year=datetime.datetime.now().year,
            month=datetime.datetime.now().month,
            day=datetime.datetime.now().day,
            hour=0
        )

    @staticmethod
    def __get_year_month_from_post(request):
        year = int(request.POST.get("year", datetime.datetime.now().year))
        month = int(request.POST.get("month", datetime.datetime.now().month))
        move = int(request.POST.get("move", 0))
        month += move
        if month == 13:
            month = 1
            year += 1
        if month == 0:
            month = 12
            year -= 1
        return year, month


# @method_decorator(token_auto_refresh, name='dispatch')
@method_decorator(pto_auth_required, name='dispatch')
@method_decorator(manager_notify, name='dispatch')
class VacationRequest(View):
    @staticmethod
    def __get_user_parameters(pto_user_id):
        current_pto_user = PTOUser.objects.get(id=pto_user_id)
        all_period_vacations_days = VacationDataBuilder.get_vacations_days(current_pto_user, ['scheduled', 'new']).days
        date = datetime.datetime.now()
        date_in_next_year = datetime.datetime(year=date.year + 1, month=1, day=1)
        next_year_days = current_pto_user.get_next_year_vacations_sum(date_in_next_year)
        total_days = current_pto_user.get_current_period_available_vacation_days()
        from PTO_project_core.helpers.PtoVacationValidationHelper import PtoVacationValidationHelper
        bonus_days = PtoVacationValidationHelper.get_bonus(current_pto_user, datetime.datetime.now())
        current_vacation_days = min(current_pto_user.get_available_vacation_days() + bonus_days, total_days)
        days_left = total_days - all_period_vacations_days - next_year_days
        current_days_left = current_vacation_days - all_period_vacations_days - next_year_days

        user_parameters = {
            'total_days': int(round(total_days)),
            'days_left': int(round(days_left)),
            'current_days_left': int(round(current_days_left)),
            'user': current_pto_user,
        }
        date_now = datetime.datetime.now()
        if current_pto_user.is_long_term(date_now) and date_now.month == 1:
            previous_period_date = current_pto_user.get_current_start_of_period_date() - datetime.timedelta(days=1)
            previous_period_days = current_pto_user.get_period_available_vacation_days_for_date(previous_period_date)
            if current_pto_user.is_third_year(datetime.datetime.now()):
                taken_days = 0
            else:
                taken_days = current_pto_user.get_vacations_sum(previous_period_date)
            if previous_period_days > taken_days:
                end_of_month = datetime.datetime(year=datetime.datetime.now().year, month=1, day=31)
                included_type_ids = VacationType.objects.filter(name=PTO_PREVIOUS_YEAR).values('id')
                statuses_ids = VacationStatus.objects.filter(text__in=['scheduled', 'new']).values('id')
                previous_year_taken_days = VacationDataBuilder.get_vacations_days_before_date(
                    current_pto_user,
                    end_of_month,
                    statuses_ids,
                    included_type_ids
                )
                if previous_period_days - taken_days - previous_year_taken_days.days > 0:
                    previous_days = int(round(previous_period_days - taken_days - previous_year_taken_days.days))
                    user_parameters['previous_year_days'] = min(
                        previous_days,
                        MAX_PTO_PREVIOUS_YEAR_DAYS - previous_year_taken_days.days
                    )
        return user_parameters

    @staticmethod
    def get(request):
        pto_user_id = request.session['pto_google_auth']['PTOUser']['id']
        form = VacationForm(initial={'pto_user_id': pto_user_id})
        parameters = VacationRequest.__get_user_parameters(pto_user_id=pto_user_id)
        parameters['form'] = form

        return render(request, 'vacation.html', parameters)

    @staticmethod
    def post(request):
        form = VacationForm(request.POST)

        if form.is_valid():
            try:
                current_pto_user = PTOUser.objects.get(email=user_session.get(request)['PTOUser']['email'])
            except PTOUser.DoesNotExist:
                return HttpResponse('User doesn`t exist!')

            vacation = Vacation(
                vacation_type=VacationType.objects.get(id=form.cleaned_data['vacation_type']),
                pto_user=current_pto_user,
                comment=form.cleaned_data['comment'],
                start_date=DateHour.build_date_time(form.cleaned_data['start_date'], form.cleaned_data['start_time']),
                end_date=DateHour.build_date_time(form.cleaned_data['end_date'], form.cleaned_data['end_time']),
                status=VacationStatus.objects.get(text='new'),
            )
            vacation.save()

            return render(request, 'thank_you_page.html', {
                'start_date': vacation.start_date,
                'end_date': vacation.end_date
            })

        else:
            pto_user_id = request.session['pto_google_auth']['PTOUser']['id']
            parameters = VacationRequest.__get_user_parameters(pto_user_id=pto_user_id)
            parameters['form'] = form
            return render(request, 'vacation.html', parameters)


# @method_decorator(token_auto_refresh, name='dispatch')
@method_decorator(pto_auth_required, name='dispatch')
class User(View):
    @staticmethod
    def get(request, user_id):
        pto_user = PTOUser.objects.get(pk=int(user_id))
        if not UserPermissions(request).has_access(pto_user):
            return HttpResponseForbidden('Sorry, but you do not have permissions to access this page.')
        employment_start_datetime = formats.date_format(
            pto_user.employment_start_datetime, 'm/d/Y') if pto_user.employment_start_datetime else None

        if pto_user.pto_user_status:
            user_status_id = pto_user.pto_user_status.id
        else:
            try:
                user_status_id = PTOUserStatus.objects.get(name='probation').id
            except PTOUserStatus.DoesNotExist:
                raise Exception('No PTOUserStatus in DB!')

        form = PTOUserForm(initial={
            'photo': pto_user.photo,
            'position': pto_user.position,
            'employment_start_datetime': employment_start_datetime,
            'pto_user_manager': pto_user.pto_user_manager,
            'pto_role': pto_user.pto_role.id,
            'pto_user_status': user_status_id
        })
        managers = PTOUser.objects.filter(
            pto_role__id=ROLE_MANAGER,
            deleted=False
        )
        return render(request, 'user.html', {
            'form': form,
            'user': pto_user,
            'managers': managers,
            'is_hr_manager': UserPermissions(request).role_id == ROLE_HR_MANAGER,
            'available_vacation_days': pto_user.get_available_vacation_days(),
        })

    @staticmethod
    def post(request, user_id):
        pto_user = PTOUser.objects.get(pk=int(user_id))
        if not UserPermissions(request).has_access(pto_user):
            return HttpResponseForbidden()
        if 'delete' in request.POST:
            pto_user.deleted = True
            pto_user.save()
            return JsonResponse({
                'status': 'ok',
            })

        if 'remove_photo' in request.POST:
            pto_user.photo = None
            pto_user.save()
            return JsonResponse({
                'status': 'ok',
            })

        form = PTOUserForm(request.POST, request.FILES)
        if form.is_valid():
            if form.cleaned_data['photo']:
                pto_user.photo = form.cleaned_data['photo']
            pto_user.position = form.cleaned_data['position']
            pto_user.employment_start_datetime = form.cleaned_data['employment_start_datetime']
            pto_user.pto_user_status = PTOUserStatus.objects.get(id=form.cleaned_data['pto_user_status'])
            if UserPermissions(request).role_id == ROLE_HR_MANAGER:
                if form.cleaned_data['pto_user_manager']:
                    pto_user.pto_user_manager = PTOUser.objects.get(pk=int(form.cleaned_data['pto_user_manager']))
                else:
                    pto_user.pto_user_manager = None
                pto_user.pto_role = PTORole.objects.get(id=form.cleaned_data['pto_role'])
            pto_user.save()
            return redirect('/#table')
        else:
            message = {
                'type': 'alert alert-danger',
                'text': 'Please fill all required fields.'
            }

        managers = PTOUser.objects.filter(
            pto_role__id=ROLE_MANAGER,
            deleted=False
        )

        return render(request, 'user.html', {
            'form': form,
            'user': pto_user,
            'managers': managers,
            'is_hr_manager': UserPermissions(request).role_id == ROLE_HR_MANAGER,
            'message': message,
        })


@method_decorator(pto_auth_required, name='dispatch')
class SetUserVacation(View):  # HR only
    @staticmethod
    def get(request, user_id):
        for_user = PTOUser.objects.get(id=user_id)
        if not UserPermissions(request).role_id == ROLE_HR_MANAGER:
            return HttpResponseForbidden()
        form = SetUserVacationForm(initial={'pto_user_id': for_user.id})
        return render(request, 'set_vacation/set_user_vacation.html', {
            'for_user': for_user,
            'form': form,
            'action_name': 'set_user_vacation',
        })

    @staticmethod
    def post(request, user_id):
        form = SetUserVacationForm(request.POST)
        try:
            for_user = PTOUser.objects.get(id=user_id)
        except PTOUser.DoesNotExist:
            return HttpResponse('User doesn`t exist!')

        if not UserPermissions(request).role_id == ROLE_HR_MANAGER:
            return HttpResponseForbidden()

        if form.is_valid():

            vacation = Vacation(
                vacation_type=VacationType.objects.get(id=form.cleaned_data['vacation_type']),
                pto_user=for_user,
                comment=form.cleaned_data['comment'],
                start_date=DateHour.build_date_time(form.cleaned_data['start_date'], 10.0),
                end_date=DateHour.build_date_time(form.cleaned_data['end_date'], 19.0),
                status=VacationStatus.objects.get(text='scheduled'),
            )
            vacation.save()
            return render(request, 'set_vacation/success_set_page.html', {
                'start_date': vacation.start_date,
                'end_date': vacation.end_date,
                'for_user': for_user,
            })
        else:
            return render(request, 'set_vacation/set_user_vacation.html', {
                'for_user': for_user,
                'form': form,
                'action_name': 'set_user_vacation',
            })


@method_decorator(pto_auth_required, name='dispatch')
class ManagerSetUserVacation(View):  # Manager only
    @staticmethod
    def get(request, user_id):
        for_user = PTOUser.objects.get(id=user_id)
        if not UserPermissions(request).has_access(for_user):
            return HttpResponseForbidden()
        form = ManagerSetUserVacationForm(initial={'pto_user_id': for_user.id})
        return render(request, 'set_vacation/set_user_vacation.html', {
            'for_user': for_user,
            'form': form,
            'action_name': 'manager_set_user_vacation',
        })

    def post(self, request, user_id):
        form = ManagerSetUserVacationForm(request.POST)
        try:
            for_user = PTOUser.objects.get(id=user_id)
        except PTOUser.DoesNotExist:
            return HttpResponse('User doesn\'t exist!')

        if not UserPermissions(request).has_access(for_user):
            return HttpResponseForbidden()

        if form.is_valid():

            vacation = Vacation(
                vacation_type=VacationType.objects.get(id=form.cleaned_data['vacation_type']),
                pto_user=for_user,
                comment=form.cleaned_data['comment'],
                start_date=DateHour.build_date_time(form.cleaned_data['start_date'], 10.0),
                end_date=DateHour.build_date_time(form.cleaned_data['end_date'], 19.0),
                status=VacationStatus.objects.get(text='scheduled'),
            )
            vacation.save()
            self.__send_letter_to_hr(request, vacation)
            return render(request, 'set_vacation/success_set_page.html', {
                'start_date': vacation.start_date,
                'end_date': vacation.end_date,
                'for_user': for_user,
            })
        else:
            return render(request, 'set_vacation/set_user_vacation.html', {
                'for_user': for_user,
                'form': form,
                'action_name': 'manager_set_user_vacation',
            })

    @staticmethod
    def __send_letter_to_hr(request, vacation):
        try:
            current_pto_user = PTOUser.objects.get(email=user_session.get(request)['PTOUser']['email'])
        except PTOUser.DoesNotExist:
            return HttpResponse('User doesn`t exist!')
        if current_pto_user.pto_role_id == 1:
            return

        managers_emails = [q.email for q in PTOUser.objects.filter(
            pto_role=ROLE_HR_MANAGER,  # , pto_company=user.pto_company
            deleted=False
        )]

        send_mail_async(
            subject='New vacation request from {} {} for user {} {}'.format(
                current_pto_user.first_name, current_pto_user.last_name,
                vacation.pto_user.first_name, vacation.pto_user.last_name),
            body=render_to_string('mail_messages/_hr_set_user_vacation_notification.txt', {
                'manager': current_pto_user,
                'vacation': vacation,
            }),
            from_email='TS PTO management tool <noreply@trustsourcing.com>',
            recipient_list=managers_emails,
            html=render_to_string('mail_messages/_hr_set_user_vacation_notification.html', {
                'manager': current_pto_user,
                'vacation': vacation,
            })
        )


# @method_decorator(token_auto_refresh, name='dispatch')
@method_decorator(vacation_notify, name='dispatch')
class VacationSetStatus(View):
    @staticmethod
    def post(request):
        if 'vacation_id' in request.POST and 'vacation_status_id' in request.POST:
            vacation = Vacation.objects.get(id=int(request.POST['vacation_id']))
            if not UserPermissions(request).has_access(vacation.pto_user):
                return HttpResponseForbidden()
            vacation.status = VacationStatus.objects.get(id=int(request.POST['vacation_status_id']))
            if 'manager_comment' in request.POST:
                vacation.manager_reason_comment = request.POST['manager_comment']
            vacation.save()
            return JsonResponse({
                'status': 'ok',
            })
        elif 'vacation_id' in request.POST and 'vacation_del' in request.POST:
            vacation = Vacation.objects.get(id=int(request.POST['vacation_id']))
            if vacation.start_date <= Dashboard.today_start() and vacation.status.text != 'new':
                return JsonResponse({
                    'status': 'failed',
                    'message': 'You can\'t delete old vacation.'
                })
            if not UserPermissions(request).is_himself(vacation.pto_user):
                return HttpResponseForbidden()
            vacation.delete()
            return JsonResponse({
                'status': 'ok',
            })
        else:
            return JsonResponse({
                'status': 'failed',
                'message': 'Missed params.'
            })


# class LoginToPTO(View):
#     @staticmethod
#     def get(request):
#         return render(request, 'login.html', {
#             'GOOGLE_CLIENT_ID': PTO_GOOGLE_CLIENT_ID,
#             'success_redirect_url': 'http://localhost:8000/dashboard/'
#         })
#
#     @staticmethod
#     def post(request):
#         if 'id_token' in request.POST and 'email' in request.POST:
#             return user_session.set(request, request.POST['email'], request.POST['id_token'])
#         else:
#             raise Exception('Token is missing!')


class Invitation(View):
    @staticmethod
    def post(request):
        if 'email' in request.POST:
            send_mail_async(
                subject='PTO invite',
                body=render_to_string(
                    'mail_messages/_invitation.txt', {'email': request.POST['email'], 'DEBUG': DEBUG}),
                from_email='TS PTO management tool <noreply@trustsourcing.com>',
                recipient_list=[request.POST['email']],
                html=render_to_string('mail_messages/invitation.html', {'email': request.POST['email'], 'DEBUG': DEBUG})
            )
        return JsonResponse({
            'status': 'ok',
        })


def logout(request):
    try:
        if 'pto_google_auth' in request.session.keys():
            credentials = client.OAuth2Credentials.from_json(
                json.dumps(request.session['pto_google_auth']['credentials']))
            # credentials.revoke(httplib2.Http())
            del request.session['pto_google_auth']
        return redirect('/')
    except (client.TokenRevokeError, client.HttpAccessTokenRefreshError, client.FlowExchangeError):
        del request.session['pto_google_auth']
        return redirect('/')


def oauth2callback(request):
    flow = get_flow(request.get_host())
    if 'code' not in request.GET:
        auth_uri = flow.step1_get_authorize_url()
        return redirect(auth_uri)
    else:
        auth_code = request.GET['code']
        credentials = flow.step2_exchange(auth_code)
        # credentials.refresh(httplib2.Http())
        save_session = user_session.set(request, credentials)
        if 'message' in save_session.keys():
            return render(request, 'login_errors.html', {
                'pto_login_error': save_session['message']
            })
        return redirect('/')


def export_user_vacations(request):
    if not UserPermissions(request).role_id == ROLE_HR_MANAGER:
        return HttpResponseForbidden('Sorry, but you do not have permissions to access this page.')

    users_vacations = [['User name', 'Email', 'Available days', 'Requested days', 'Scheduled days', 'Taken days',
                        'Vacation type', 'Vacation start day', 'Vacation end date', 'days', 'status']]
    users = PTOUser.objects.filter(
        deleted=False
    )
    UsersHelper().calculate_days(users)
    for user in users:
        row = [user.first_name + ' ' + user.last_name, user.email, user.available_vacation_days.days,
               user.requested_days.plaint(), user.scheduled_days.plaint(), user.taken_days.plaint()]
        vacations = Vacation.objects.filter(
            pto_user=PTOUser.objects.get(id=user.id))
        if len(vacations) == 0:
            users_vacations.append(row)
        for vacation in vacations:
            working_day_for_range = CalendarBuilder.count_working_day_for_range(vacation.start_date, vacation.end_date)
            row.extend([vacation.vacation_type.name, vacation.start_date.strftime('%d.%m.%Y'),
                        vacation.end_date.strftime('%d.%m.%Y'), working_day_for_range.plaint(), vacation.status.text])
            users_vacations.append(row)
            row = ['', '', '', '', '', '']

    today = datetime.date.today().strftime('-%d-%m-%Y').replace('-0', '-').strip('-')
    return excel.make_response_from_array(users_vacations, 'xlsx', file_name="users vacations " + today)

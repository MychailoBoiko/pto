import json

import datetime

from PTO_project_core.helpers.CalendarBuilder import DateHour
from PTO_project_core.vacations_data_builder.VacationDataBuilder import VacationDataBuilder
from PTO_project_core.models import Vacation, VacationType, VacationStatus
from PTO_project_core.helpers.CalendarBuilder import CalendarBuilder

PAID_VACATION_NAME = 'Paid vacation'
HIDDEN_VACATION_NAMES = ['Other', 'Compassionate leave', 'Force Majeure']
SICK_LEAVE_VACATION_NAME = 'Sick leave'
NON_PAID_VACATION_NAME = 'Non-paid vacation'
WORK_FROM_HOME_VACATION_NAME = 'Work from Home'
FORCE_MAJEURE_VACATION_NAME = 'Force Majeure'
PTO_PREVIOUS_YEAR = 'PTO previous year (January only)'


class UsersHelper(object):
    def __init__(self):
        self.new_statuses = [val['id'] for val in VacationStatus.objects.filter(
            text__in=['new']).values('id')]
        self.scheduled_statuses = [val['id'] for val in VacationStatus.objects.filter(
            text__in=['scheduled']).values('id')]
        self.scheduled_new_statuses = [val['id'] for val in VacationStatus.objects.filter(
            text__in=['new', 'scheduled']).values('id')]
        self.included_type_ids = [val['id'] for val in VacationType.objects.filter(
            name__in=[PAID_VACATION_NAME, PTO_PREVIOUS_YEAR]).values('id')]
        self.previous_year_type_ids = [val['id'] for val in VacationType.objects.filter(
            name__in=[PTO_PREVIOUS_YEAR]).values('id')]
        self.paid_type_ids = [val['id'] for val in VacationType.objects.filter(
            name__in=[PAID_VACATION_NAME]).values('id')]

        self.sick_vacation_type_ids = [val['id'] for val in VacationType.objects.filter(
            name__in=[SICK_LEAVE_VACATION_NAME]).values('id')]
        self.hidden_vacation_type_ids = [val['id'] for val in VacationType.objects.filter(
            name__in=HIDDEN_VACATION_NAMES).values('id')]
        self.non_paid_vacation_type_ids = [val['id'] for val in VacationType.objects.filter(
            name__in=[NON_PAID_VACATION_NAME]).values('id')]
        self.work_from_home_vacation_type_ids = [val['id'] for val in VacationType.objects.filter(
            name__in=[WORK_FROM_HOME_VACATION_NAME]).values('id')]

    def calculate_days(self, users):
        """
        :param users: PTOUser QuerySet
        :return: dict
        """
        totals = {
            'available_vacation_days': DateHour(),
            'current_vacation_days': DateHour(),
            'requested_days': DateHour(),
            'scheduled_days': DateHour(),
            'taken_days': DateHour(),
            'sick': DateHour(),
            'other': DateHour(),
            'non_paid': DateHour(),
            'home': DateHour(),
        }

        for user in users:
            current_date = user.get_current_start_of_period_date()
            vacations = Vacation.objects.filter(
                pto_user=user.id,
                end_date__gte=current_date,
                start_date__lte=user.get_current_end_of_period_date()
            ).exclude(vacation_type__in=self.previous_year_type_ids)
            previous_year_vacations = Vacation.objects.filter(
                pto_user=user.id,
                end_date__gte=datetime.datetime(year=current_date.year + 1, month=1, day=1),
                start_date__lte=datetime.datetime(year=current_date.year + 1, month=1, day=31),
                vacation_type__in=self.previous_year_type_ids
            )
            vacations = vacations | previous_year_vacations
            user.requested_days = VacationDataBuilder.get_vacations_days_with_previous_year(
                user,
                vacations,
                self.new_statuses,
                self.included_type_ids
            )
            user.scheduled_days = VacationDataBuilder.get_vacations_days_with_previous_year(
                user,
                vacations,
                self.scheduled_statuses,
                self.included_type_ids
            )
            user.taken_days = VacationDataBuilder.get_vacations_days_before_date(
                user,
                datetime.datetime.now(),
                self.scheduled_new_statuses,
                self.included_type_ids
            )
            all_taken_days = VacationDataBuilder.get_vacations_days_with_previous_year(
                user,
                vacations,
                self.scheduled_new_statuses,
                self.included_type_ids
            )
            vacation_days = max(user.get_current_period_available_vacation_days(), 0)
            all_available_vacation_days = DateHour(days=vacation_days).minus(all_taken_days)

            user.sick = VacationDataBuilder.get_static_vacations(
                user, vacations, self.scheduled_statuses, self.sick_vacation_type_ids)
            user.other = VacationDataBuilder.get_static_vacations(
                user, vacations, self.scheduled_statuses, self.hidden_vacation_type_ids)
            user.non_paid = VacationDataBuilder.get_static_vacations(
                user, vacations, self.scheduled_statuses, self.non_paid_vacation_type_ids)
            user.home = VacationDataBuilder.get_static_vacations(
                user, vacations, self.scheduled_statuses, self.work_from_home_vacation_type_ids)

            user.available_vacation_days = all_available_vacation_days

            from PTO_project_core.helpers.PtoVacationValidationHelper import PtoVacationValidationHelper
            last_date = self.__get_last_date(vacations)
            bonus_days = PtoVacationValidationHelper.get_bonus(user, last_date)
            last_date_current_vacation_days = min(
                user.get_period_available_vacation_days_for_date(last_date) + bonus_days,
                vacation_days
            )
            taken_days = VacationDataBuilder.get_vacations_days_before_date(
                user,
                datetime.datetime.now(),
                self.scheduled_new_statuses,
                self.included_type_ids
            )
            current_bonus_days = PtoVacationValidationHelper.get_bonus(user, datetime.datetime.now())
            today__current_vacation_days = min(
                user.get_period_available_vacation_days_for_date(datetime.datetime.now()) + current_bonus_days,
                vacation_days
            )

            current_vacation_days = min(
                last_date_current_vacation_days - all_taken_days.days,
                today__current_vacation_days - taken_days.days,
            )
            current_vacation_days = max(current_vacation_days,0)
            user.current_vacation_days = DateHour(days=current_vacation_days)

            totals['requested_days'].add_days_hours(user.requested_days)
            totals['scheduled_days'].add_days_hours(user.scheduled_days)
            totals['sick'].add_days_hours(user.sick)
            totals['other'].add_days_hours(user.other)
            totals['non_paid'].add_days_hours(user.non_paid)
            totals['home'].add_days_hours(user.home)

            totals['available_vacation_days'].add_days_hours(user.available_vacation_days)
            totals['current_vacation_days'].add_days_hours(user.current_vacation_days)

            user.vacations = self.get_user_vacations_json(user, vacations)

        return totals

    def get_user_vacations_json(self, user_model, vacations):
        vacations_lists = {
            'new': [],
            'scheduled': [],
            'sick': [],
            'other': [],
            'non_paid': [],
            'home': [],
        }

        for vacation in vacations:
            if vacation.status_id in self.new_statuses and vacation.vacation_type_id in self.included_type_ids:
                vacations_lists['new'].append(vacation)
            elif vacation.status_id in self.scheduled_statuses:
                if vacation.vacation_type_id in self.included_type_ids:
                    vacations_lists['scheduled'].append(vacation)
                elif vacation.vacation_type_id in self.sick_vacation_type_ids:
                    vacations_lists['sick'].append(vacation)
                elif vacation.vacation_type_id in self.hidden_vacation_type_ids:
                    vacations_lists['other'].append(vacation)
                elif vacation.vacation_type_id in self.non_paid_vacation_type_ids:
                    vacations_lists['non_paid'].append(vacation)
                elif vacation.vacation_type_id in self.work_from_home_vacation_type_ids:
                    vacations_lists['home'].append(vacation)

        vacations_lists_json = {}
        for column in vacations_lists:
            vacations = vacations_lists[column]
            vacations_lists_json[column] = json.dumps(
                [
                    {
                        'start_date': v.start_date.strftime('%m/%d/%Y'),
                        'end_date': v.end_date.strftime('%m/%d/%Y'),
                        'comment': v.comment,
                        'days': CalendarBuilder.count_working_day_for_range(
                            v.start_date, v.end_date).days,
                        'hours': CalendarBuilder.count_working_day_for_range(
                            v.start_date, v.end_date).hours,
                        'reason': v.manager_reason_comment if v.manager_reason_comment else 'Not specified.',
                        'vacation_type': v.vacation_type.name
                    }
                    for v in vacations
                    ]
            )

        return vacations_lists_json

    @staticmethod
    def __get_last_date(vacations):
        date = datetime.datetime.now()
        for vacation in vacations:
            date = max(date, vacation.end_date)
        return date

from PTO_project_core.models import PTOUser

ROLE_HR_MANAGER = 1
ROLE_USER = 2
ROLE_MANAGER = 3


class UserPermissions(object):
    current_user = None

    def __init__(self, request):
        self.current_user = PTOUser.objects.get(id=request.session['pto_google_auth']['PTOUser']['id'])

    @property
    def role_id(self):
        return self.current_user.pto_role_id

    def has_access(self, user):
        # Turned off because of cancelling checking company
        # if self.current_user.pto_company_id != user.pto_company_id:
        #     return False
        if self.current_user.pto_role_id == ROLE_HR_MANAGER:
            return True
        elif self.current_user.pto_role_id == ROLE_MANAGER:
            if self.current_user.id == user.pto_user_manager_id:
                return True
        return False

    def is_himself(self, user):
        return self.current_user.id == user.id

    def has_access_or_himself(self, user):
        return self.has_access(user) or self.is_himself(user)

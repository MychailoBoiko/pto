import datetime
from calendar import monthrange

from PTO_project_core.helpers.CalendarBuilder import CalendarBuilder
from PTO_project_core.helpers.UsersHelper import NON_PAID_VACATION_NAME, WORK_FROM_HOME_VACATION_NAME, \
    PAID_VACATION_NAME, FORCE_MAJEURE_VACATION_NAME, PTO_PREVIOUS_YEAR
from PTO_project_core.models import VacationType, Vacation, VacationStatus, DAYS_IN_HALF_YEAR
from PTO_project_core.vacations_data_builder.VacationDataBuilder import VacationDataBuilder

AVAILABLE_DAYS_IN_ONE_REQUEST = 5
NON_PAID_VACATION_MAX_DAYS = 30
MAX_WORK_FROM_HOME_DAYS = 31
MAX_FORCE_MAJEURE_DAYS = 5
MAX_PTO_PREVIOUS_YEAR_DAYS = 5


class PtoVacationValidationHelper(object):
    user = None
    start_date = None
    end_date = None
    form = None

    def __init__(self, form, user, start_date, end_date):
        self.user = user
        self.start_date = start_date
        self.end_date = end_date
        self.form = form

    @staticmethod
    def get_bonus(user, date):
        return 5 if user.is_long_term(date) else 1

    def pto_validate(self, v_type):
        if v_type.name == PAID_VACATION_NAME:
            requested_days_diff = CalendarBuilder.count_working_day_for_range(self.start_date, self.end_date)
    
            if not self.__validate_max_days(requested_days_diff):
                return False
    
            sum_scheduled_days = self.user.get_vacations_sum(self.end_date)
            date_in_next_year = datetime.datetime(year=self.end_date.year + 1, month=1, day=1)
            next_year_days = self.user.get_next_year_vacations_sum(date_in_next_year)
            sum_scheduled_days += next_year_days
            bonus = self.get_bonus(self.user, self.end_date)
    
            return self.__validate_period(requested_days_diff.days, self.end_date, bonus, sum_scheduled_days)
        
        return True

    def previous_year_pto_validate(self, v_type):
        if v_type.name == PTO_PREVIOUS_YEAR:
            date_interval = self.end_date - self.user.employment_start_datetime
            if date_interval.days < DAYS_IN_HALF_YEAR:
                self.form.add_error(
                   'end_date',
                   'You can\'t take vacation during half year probation period'
                )
                return False
            date_in_previous_year = datetime.datetime(year=self.end_date.year - 1, month=12, day=31)
            if self.user.is_first_year(date_in_previous_year):
                self.form.add_error('vacation_type',
                                    'Previous year days will be available only next year in January for you.')
                return False

            if self.start_date.month != 1:
                self.form.add_error('start_date', 'Start date must be in January')
                return False
            if self.end_date.month != 1:
                self.form.add_error('end_date', 'End date must be in January')
                return False

            requested_days_diff = CalendarBuilder.count_working_day_for_range(self.start_date, self.end_date)

            date_in_next_year = datetime.datetime(year=self.end_date.year, month=1, day=1)
            next_year_days = self.user.get_next_year_vacations_sum(date_in_next_year)

            if requested_days_diff.days + next_year_days > MAX_PTO_PREVIOUS_YEAR_DAYS:
                self.form.add_error(
                    'start_date', 'You can\'t take more then {} PTO previous year days.'.format(
                        MAX_PTO_PREVIOUS_YEAR_DAYS)
                )
                return False

            if self.user.is_third_year(date_in_next_year):
                taken_days = 0
            else:
                taken_days = self.user.get_vacations_sum(date_in_previous_year)
            previous_available_days = self.user.get_period_available_vacation_days_for_date(date_in_previous_year)
            days_left = previous_available_days - next_year_days - taken_days
            if requested_days_diff.days > days_left:
                self.form.add_error(
                    'start_date', 'You have {} days available from previous year Please fit your request.'.format(
                        int(round(days_left)))
                )
                return False

            if not self.__validate_max_days(requested_days_diff):
                return False

        return True

    def __get_previous_period(self):
        current_period_date = self.user.get_start_of_period_date(self.end_date)
        previous_date = current_period_date - datetime.timedelta(1)
        return [self.user.get_start_of_period_date(previous_date), self.user.get_end_of_period_date(previous_date)]

    def __validate_period(self, days, end_date, bonus, sum_scheduled_days):
        date_interval = end_date - self.user.employment_start_datetime
        if date_interval.days < DAYS_IN_HALF_YEAR:
            self.form.add_error(
               'end_date',
               'You can\'t take vacation during half year probation period'
            )
            return False

        period_end_date = self.user.get_end_of_period_date(end_date)
        if days > self.user.get_period_available_vacation_days_for_date(period_end_date) - sum_scheduled_days:
            self.form.add_error(
                'start_date',
                'You have {} days available for paid vacation by the end of year. Please fit your request.'.format(
                    self.user.get_period_available_vacation_days_for_date(period_end_date) - sum_scheduled_days
                )
            )
            return False
        from PTO_project_core.helpers.UsersHelper import PAID_VACATION_NAME
        included_type_ids = VacationType.objects.filter(name=PAID_VACATION_NAME).values('id')
        statuses_ids = VacationStatus.objects.filter(text__in=['scheduled']).values('id')
        from PTO_project_core.vacations_data_builder.VacationDataBuilder import VacationDataBuilder
        days_sum_for_date = VacationDataBuilder.get_vacations_days_before_date(
            self.user,
            end_date,
            statuses_ids,
            included_type_ids
        )
        collected_by_end_of_vacation = self.user.get_period_available_vacation_days_for_date(end_date)

        last_date_current_period = self.__get_last_vacation_date(period_end_date)
        last_date = last_date_current_period
        if self.user.is_first_year(self.end_date):
            next_period_end_date = self.user.get_end_of_next_period_date(self.end_date)
            sum_scheduled_days += self.user.get_vacations_sum(next_period_end_date)
            last_date = self.__get_last_vacation_date(next_period_end_date)

        collected_by_end_of_last_vacation = self.user.get_period_available_vacation_days_for_date(
            last_date_current_period)
        if last_date > last_date_current_period:
            collected_by_end_of_last_vacation = self.user.get_period_available_vacation_days_for_date(
                period_end_date) + self.user.get_period_available_vacation_days_for_date(last_date) - days - bonus
        available_days = min(
            collected_by_end_of_vacation + bonus - days_sum_for_date.days,
            collected_by_end_of_last_vacation + bonus - sum_scheduled_days
        )

        if days > available_days:
            self.form.add_error(
                'end_date',
                'You have {} days available by the end of vacation time period. Please fit your request.'.format(
                    available_days
                )
            )
            return False
        return True

    def __validate_max_days(self, requested_days_diff):
        if requested_days_diff.days > AVAILABLE_DAYS_IN_ONE_REQUEST:
            self.form.add_error(
                'start_date',
                'You can\'t take more than {} days in one PTO request. Please fit your request.'.format(
                    AVAILABLE_DAYS_IN_ONE_REQUEST
                )
            )
            return False
        return True

    def __get_last_vacation_date(self, period_end_date):
        last_date = self.end_date
        try:
            from PTO_project_core.helpers.UsersHelper import PAID_VACATION_NAME
            statuses = ['new', 'scheduled']
            last_vacation = Vacation.objects.filter(
                pto_user__id=self.user.id,
                status__text__in=statuses,
                vacation_type__name=PAID_VACATION_NAME,
                end_date__lte=period_end_date
            ).order_by('-end_date')[0]
            last_date = max(last_date, last_vacation.end_date)
        except IndexError:
            pass
        return last_date

    def intersection_validate(self):
        existent_vacation_requests = Vacation.objects.filter(
            pto_user__id=self.user.id).exclude(status__text='rejected')

        for vacation_request in existent_vacation_requests:
            vacation_start_date = datetime.datetime(
                year=vacation_request.start_date.year,
                month=vacation_request.start_date.month,
                day=vacation_request.start_date.day
            )
            vacation_end_date = datetime.datetime(
                year=vacation_request.end_date.year,
                month=vacation_request.end_date.month,
                day=vacation_request.end_date.day
            )
            if vacation_start_date <= self.start_date <= vacation_end_date or \
               vacation_start_date <= self.end_date <= vacation_end_date or \
               self.start_date <= vacation_start_date <= self.end_date:
                self.form.add_error('start_date', 'Intersection of date range with your already existent requests!')
                return False

        return True

    def base_validate(self):
        if self.end_date < self.start_date:
            self.form.add_error('end_date', 'End date must be not less than the start date!')
            return False

        if self.end_date.year != self.start_date.year:
            self.form.add_error('end_date', 'Dates must be in one year. Please split your vacations!')
            return False

        if self.end_date.year > datetime.datetime.now().year + 1:
            self.form.add_error('end_date', 'You can\'t take day off later then next year!')
            return False

        return True

    def not_paid_validate(self, v_type):
        if v_type.name == NON_PAID_VACATION_NAME:
            start_date = self.user.get_start_of_period_date(self.end_date)
            end_date = self.user.get_end_of_period_date(self.end_date)
            vacations = Vacation.objects.filter(
                start_date__lte=end_date,
                end_date__gte=start_date,
                pto_user_id=self.user.id,
                vacation_type__name=NON_PAID_VACATION_NAME
            )
            total_taken_days = VacationDataBuilder.get_days_hours_for_vacations(vacations)
            day_for_range = CalendarBuilder.count_working_day_for_range(self.start_date,
                                                                        self.end_date).days
            if day_for_range > NON_PAID_VACATION_MAX_DAYS - total_taken_days.days:
                self.form.add_error(
                    'start_date',
                    'You can\'t take {} \'{}\' working days. {} days (from {}) left'.format(
                        NON_PAID_VACATION_NAME,
                        day_for_range,
                        NON_PAID_VACATION_MAX_DAYS - total_taken_days.days,
                        NON_PAID_VACATION_MAX_DAYS
                    )
                )
                return False

        return True

    def work_from_home_validate(self, v_type):
        if v_type.name == WORK_FROM_HOME_VACATION_NAME:
            return self.__validate_one_month(WORK_FROM_HOME_VACATION_NAME, MAX_WORK_FROM_HOME_DAYS)
        return True
    
    def force_majeure_validate(self, v_type):
        if v_type.name == FORCE_MAJEURE_VACATION_NAME:
            return self.__validate_one_month(FORCE_MAJEURE_VACATION_NAME, MAX_FORCE_MAJEURE_DAYS)
        self.form.add_error(
            'vacation_type',
            'You can\'t set only {} vacation type.'.format(FORCE_MAJEURE_VACATION_NAME)
        )
        return False

    def __validate_one_month(self, vacation_type_name, max_days):
        now = datetime.datetime.now()
        if self.start_date.month != now.month:
            self.form.add_error(
                'start_date',
                'You should take \'{}\' days only in current month.'.format(vacation_type_name)
            )
            return False

        if self.end_date.month != now.month:
            self.form.add_error(
                'end_date',
                'You should take \'{}\' days only in current month.'.format(vacation_type_name)
            )
            return False

        week_day, last_day = monthrange(now.year, now.month)
        end_of_month = datetime.datetime(
            year=now.year, month=now.month, day=last_day, hour=23, minute=59, second=59)
        start_of_month = datetime.datetime(
            year=now.year, month=now.month, day=1)
        statuses = ['new', 'scheduled']
        vacations = Vacation.objects.filter(
            start_date__lte=end_of_month.strftime("%Y-%m-%d %H:%M:%S"),
            end_date__gte=start_of_month.strftime("%Y-%m-%d %H:%M:%S"),
            pto_user_id=self.user.id,
            status__text__in=statuses,
            vacation_type__name=vacation_type_name
        ).order_by('start_date')
        current_from_home_vacation_days = 0
        for vacation in vacations:
            current_start_date = max(start_of_month, vacation.start_date)
            current_end_date = min(end_of_month, vacation.end_date)
            current_from_home_vacation_days += CalendarBuilder.count_working_day_for_range(current_start_date,
                                                                                           current_end_date).days
        vacation_days = CalendarBuilder.count_working_day_for_range(self.start_date, self.end_date).days
        if vacation_days + current_from_home_vacation_days > max_days:
            self.form.add_error(
                'start_date',
                'You can\'t take more than {} \'{}\' days in one month. {} already taken.'.format(
                    max_days, vacation_type_name, current_from_home_vacation_days
                )
            )
            return False
        return True

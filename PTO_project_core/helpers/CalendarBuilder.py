import datetime
from calendar import Calendar
from calendar import *

from PTO_project_core.models import Holiday

START_WORKING_HOUR = 10
END_WORKING_HOUR = 19
WORKING_HOURS = 8
LUNCH_TIME = 1
HOURS_FOR_SELECT = (
    (10, '10:00 AM'),
    (10.5, '10:30 AM'),
    (11, '11:00 AM'),
    (11.5, '11:30 AM'),
    (12, '12:00 PM'),
    (12.5, '12:30 PM'),
    (13, '1:00 PM'),
    (13.5, '1:30 PM'),
    (14, '2:00 PM'),
    (14.5, '2:30 PM'),
    (15, '3:00 PM'),
    (15.5, '3:30 PM'),
    (16, '4:00 PM'),
    (16.5, '4:30 PM'),
    (17, '5:00 PM'),
    (17.5, '5:30 PM'),
    (18, '6:00 PM'),
    (18.5, '6:30 PM'),
    (19, '7:00 PM')
)


class CalendarBuilder(object):
    DAYS_IN_WEEK = 7
    START_DAY = 0
    WEEK_DAYS_HEADER = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY']
    WEEK_DAYS_HEADER_SHORT = ['MO', 'TU', 'WE', 'TH', 'FR', 'SA', 'SU']
    is_current_month = False
    current_day = None
    month = None
    year = None
    calendar = None
    title = None
    table_calendar = {}
    table_calendar_header = {}

    def __init__(self, _year=datetime.datetime.now().year, _month=datetime.datetime.now().month):
        self.current_day = datetime.datetime.now().day

        self.month = _month
        self.year = _year

        self.__set_title()
        self.__set_is_current_month()
        self.__set_table_calendar_values()

        calendar_object = Calendar()
        calendar_object.setfirstweekday(self.START_DAY)
        calendar_list = calendar_object.monthdayscalendar(self.year, self.month)
        self.calendar = [[0 for j in range(7)] for i in range(calendar_list.__len__())]
        i = 0
        for week in calendar_list:
            j = 0
            for day in week:
                if day > 0:
                    day_type, description = self.__get_day_type_description(day)
                    self.calendar[i][j] = {
                        'day': day,
                        'type': day_type,
                        'description': description,
                        'is_current_day': True if day == self.current_day and self.is_current_month else False,
                        'other_month': False,
                    }
                else:
                    if i == 0:
                        self.calendar[i][j] = self.__get_previous_month_day(j)
                    else:
                        self.calendar[i][j] = self.__get_next_month_day(j)
                j += 1
            i += 1

    def __set_is_current_month(self):
        if self.month == datetime.datetime.now().month and self.year == datetime.datetime.now().year:
            self.is_current_month = True

    def __set_title(self):
        my_date = datetime.datetime(year=self.year, month=self.month, day=1)
        self.title = my_date.strftime("%B") + ' ' + str(self.year)

    def __set_table_calendar_values(self):
        self.table_calendar = {}
        self.table_calendar_header = {}
        week_day, to_day = monthrange(self.year, self.month)
        for day in range(1, to_day + 1):
            day_header = self.WEEK_DAYS_HEADER_SHORT[weekday(self.year, self.month, day)]
            day_type, description = self.__get_day_type_description(day)
            self.table_calendar_header[day] = {
                'name': day_header,
                'type': day_type,
                'description': description,
            }
            self.table_calendar[day] = {
                'day': day,
                'type': day_type,
                'description': description,
            }

    def __is_working_day_description(self, day, description):
        is_working_day = CalendarBuilder.is_working_day(self.year, self.month, day, description)
        return is_working_day

    @staticmethod
    def is_working_day(current_year, current_month, current_day, description=None):
        counter = weekday(current_year, current_month, current_day)
        try:
            holiday = Holiday.objects.get(holiday_date=datetime.date(current_year, current_month, current_day))
            if description is not None:
                description['name'] = holiday.holiday_name
            if holiday.is_working_day:
                return True
            return False
        except Holiday.DoesNotExist:
            pass
        except Holiday.MultipleObjectsReturned:
            holidays = Holiday.objects.filter(holiday_date=datetime.date(current_year, current_month, current_day))
            if description is not None:
                description['name'] = ','.join([h.holiday_name for h in holidays])
            for h in holidays:
                if h.is_working_day:
                    return True
            return False
        if counter > 4:
            return False
        return True

    def __get_day_type_description(self, day):
        description = {'name': ''}
        is_working_day = self.__is_working_day_description(day, description)
        day_type = 'working-day' if is_working_day else 'not-working-day'
        return day_type, description['name']

    @staticmethod
    def count_working_day_for_range(start_date, end_date):
        hours = 0
        days = 0
        # start_date_hour = float(start_date.hour)
        # if start_date.minute > 0:
        #     start_date_hour += float(start_date.minute) / 60
        # end_date_hour = float(end_date.hour)
        # if end_date.minute > 0:
        #     end_date_hour += float(end_date.minute) / 60
        # if (end_date - start_date).days == 0 and CalendarBuilder.is_working_day(
        #             start_date.year, start_date.month, start_date.day):
        #     hours += CalendarBuilder.get_hours_interval(start_date_hour, end_date_hour)
        #     days -= 1
        # else:
        #     if start_date_hour > START_WORKING_HOUR and CalendarBuilder.is_working_day(
        #             start_date.year, start_date.month, start_date.day):
        #         hours += CalendarBuilder.get_hours_interval(start_date_hour, END_WORKING_HOUR)
        #         days -= 1
        #     if end_date_hour < END_WORKING_HOUR and CalendarBuilder.is_working_day(
        #             end_date.year, end_date.month, end_date.day):
        #         hours += CalendarBuilder.get_hours_interval(START_WORKING_HOUR, end_date_hour)
        #         days -= 1
        dates = CalendarBuilder.__date_range(start_date, end_date)
        for date in dates:
            if CalendarBuilder.is_working_day(date.year, date.month, date.day):
                days += 1
        days += int(hours) / WORKING_HOURS
        hours -= (int(hours) / WORKING_HOURS) * WORKING_HOURS
        return DateHour(days, hours)

    @staticmethod
    def __date_range(start_date, end_date):
        for n in range(int((end_date - start_date).days) + 1):
            yield start_date + datetime.timedelta(n)

    def __get_previous_month_day(self, j):
        previous_month = self.month - 1
        year_for_month = self.year
        if previous_month == 0:
            previous_month = 12
            year_for_month -= 1
        calendar_object = Calendar()
        calendar_object.setfirstweekday(self.START_DAY)
        calendar_list = calendar_object.monthdayscalendar(year_for_month, previous_month)
        other_day = calendar_list[len(calendar_list) - 1][j]
        if CalendarBuilder.is_working_day(year_for_month, previous_month, other_day):
            day_type = 'working-day'
        else:
            day_type = 'not-working-day'
        return {
            'day': other_day,
            'type': day_type,
            'other_month': True,
        }

    def __get_next_month_day(self, j):
        next_month = self.month + 1
        year_for_month = self.year
        if next_month == 13:
            next_month = 1
            year_for_month += 1
        calendar_object = Calendar()
        calendar_object.setfirstweekday(self.START_DAY)
        calendar_list = calendar_object.monthdayscalendar(year_for_month, next_month)
        other_day = calendar_list[0][j]
        if CalendarBuilder.is_working_day(year_for_month, next_month, other_day):
            day_type = 'working-day'
        else:
            day_type = 'not-working-day'
        return {
            'day': other_day,
            'type': day_type,
            'other_month': True,
        }

    @staticmethod
    def get_hours_interval(start_hour, end_hour):
        interval = end_hour - start_hour
        if interval > (END_WORKING_HOUR - START_WORKING_HOUR)/2:
            interval -= 1
        return interval


class DateHour(object):
    days = 0
    hours = 0

    def __init__(self, days=0, hours=0):
        self.days = days
        self.hours = hours

    def add_days_hours(self, days_hours):
        self.days += days_hours.days
        self.hours += days_hours.hours

    @staticmethod
    def build_date_time(input_date, input_time):
        date_time = input_date
        time_delta = datetime.timedelta(hours=input_time)
        date_time += time_delta
        return date_time

    def plaint(self):
        plaint = str(self)
        plaint = plaint.replace('<br>', ' ')
        return plaint

    def simplify(self):
        self.days += int(self.hours) / WORKING_HOURS
        self.hours -= (int(self.hours) / WORKING_HOURS) * WORKING_HOURS

    def __str__(self):
        self.simplify()
        return str(int(round(self.days)))

    def minus(self, days_hours):
        self.simplify()
        days_hours.simplify()
        resulted_days = self.days - days_hours.days
        resulted_hours = self.hours - days_hours.hours
        if resulted_hours < 0:
            resulted_hours += WORKING_HOURS
            resulted_days -= 1
        return DateHour(resulted_days, resulted_hours)

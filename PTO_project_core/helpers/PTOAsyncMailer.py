# -*- coding: utf-8 -*-
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from functools import wraps
import threading

from PTO_project_core.helpers.CalendarBuilder import CalendarBuilder
from PTO_project_core.helpers.CalendarBuilder import DateHour
from PTO_project_core.models import Vacation, VacationStatus, PTOUser, VacationType
from PTO_project_core.forms import VacationForm
from PTO_project_core.helpers.UserPermissions import ROLE_HR_MANAGER


class EmailThread(threading.Thread):
    def __init__(self, subject, body, from_email, recipient_list, fail_silently, html):
        self.subject = subject
        self.body = body
        self.recipient_list = recipient_list
        self.from_email = from_email
        self.fail_silently = fail_silently
        self.html = html
        threading.Thread.__init__(self)

    def run(self):
        msg = EmailMultiAlternatives(self.subject, self.body, self.from_email, self.recipient_list)
        if self.html:
            msg.attach_alternative(self.html, "text/html")
        msg.send(self.fail_silently)


def send_mail_async(subject, body, from_email, recipient_list, fail_silently=False, html=None):
    EmailThread(subject, body, from_email, recipient_list, fail_silently, html).start()


def vacation_notify(view_func):
    @wraps(view_func)
    def decorated(request, *args, **kwargs):

        if 'vacation_id' in request.POST and 'vacation_status_id' in request.POST:
            status = VacationStatus.objects.get(id=request.POST['vacation_status_id'])
            vacation = Vacation.objects.get(id=request.POST['vacation_id'])
            mail_reason_subj = None
            if status.text == 'scheduled':
                mail_reason_subj = 'Your vacation request was scheduled.'
            elif status.text == 'rejected':
                mail_reason_subj = 'Your vacation request was rejected.'

            reject_reason = request.POST['manager_comment'] if 'manager_comment' in request.POST else None

            html_template = render_to_string('mail_messages/notification.html', {
                'vacation': vacation,
                'status': status.text,
                'reject_reason': reject_reason
            })

            send_mail_async(
                subject=mail_reason_subj,
                body=render_to_string('mail_messages/_notification.txt', {
                    'vacation': vacation, 'status': status.text, 'reject_reason': reject_reason}),
                from_email='TS PTO management tool <noreply@trustsourcing.com>',
                recipient_list=[vacation.pto_user.email],
                html=html_template
            )
        elif 'vacation_id' in request.POST and 'vacation_del' in request.POST:
            user = PTOUser.objects.get(id=request.session['pto_google_auth']['PTOUser']['id'])
            vacation = Vacation.objects.get(id=request.POST['vacation_id'])
            managers_emails = [q.email for q in PTOUser.objects.filter(
                pto_role=ROLE_HR_MANAGER,  # , pto_company=user.pto_company
                deleted=False
            )]

            if user.pto_user_manager and user.pto_user_manager.email not in managers_emails:
                managers_emails.append(user.pto_user_manager.email)

            html_template = render_to_string('mail_messages/vacation_deleted.html', {
                'vacation': vacation
            })

            send_mail_async(
                subject='Vacation request was deleted.',
                body=render_to_string('mail_messages/_vacation_deleted.txt', {'vacation': vacation}),
                from_email='TS PTO management tool <noreply@trustsourcing.com>',
                recipient_list=managers_emails,
                html=html_template
            )

        return view_func(request, *args, **kwargs)

    return decorated


def manager_notify(view_func):
    @wraps(view_func)
    def decorated(request, *args, **kwargs):
        form = VacationForm(request.POST)
        if form.is_valid():
            user = PTOUser.objects.get(id=form.cleaned_data['pto_user_id'])

            managers_emails = [q.email for q in PTOUser.objects.filter(
                pto_role=ROLE_HR_MANAGER,  # , pto_company=user.pto_company
                deleted=False
            )]

            if user.pto_user_manager and user.pto_user_manager.email not in managers_emails:
                managers_emails.append(user.pto_user_manager.email)

            start_date_time = DateHour.build_date_time(form.cleaned_data['start_date'], form.cleaned_data['start_time'])
            end_date_time = DateHour.build_date_time(form.cleaned_data['end_date'], form.cleaned_data['end_time'])
            total_days = CalendarBuilder.count_working_day_for_range(form.cleaned_data['start_date'],
                                                                     form.cleaned_data['end_date']).days
            vacation_type_name_ = VacationType.objects.get(id=form.cleaned_data['vacation_type']).name
            email_parameters = {
                'user': user,
                'comment': form.cleaned_data['comment'],
                'start_date': start_date_time,
                'end_date': end_date_time,
                'total_days': total_days,
                'vacation_type_name': vacation_type_name_
            }
            send_mail_async(
                subject='New vacation request from {} {}'.format(user.first_name, user.last_name),
                body=render_to_string('mail_messages/_manager_notification.txt', email_parameters),
                from_email='TS PTO management tool <noreply@trustsourcing.com>',
                recipient_list=managers_emails,
                html=render_to_string('mail_messages/manager_notification.html', email_parameters)
            )

        return view_func(request, *args, **kwargs)

    return decorated

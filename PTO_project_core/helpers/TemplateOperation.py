import math


class TemplateOperation(object):

    @staticmethod
    def split_into_columns(input_list, columns_number):
        input_list_length = input_list.__len__()
        column_list_count = int(math.ceil(float(input_list_length) / float(columns_number)))
        column_list_count_small = column_list_count - 1
        big_columns_number = column_list_count * columns_number - input_list_length
        current_length = 0
        current_column = 0
        resulted_list = []
        column_list = []
        for element in input_list:
            column_list.append(element)
            current_length += 1
            if current_column == big_columns_number and big_columns_number > 0:
                column_list_count = column_list_count_small
            if current_length == column_list_count:
                current_column += 1
                resulted_list.append(column_list)
                column_list = []
                current_length = 0
        return resulted_list

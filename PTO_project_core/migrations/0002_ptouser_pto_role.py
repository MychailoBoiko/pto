# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-25 16:06
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('PTO_project_core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='ptouser',
            name='pto_role',
            field=models.ForeignKey(default=2, on_delete=django.db.models.deletion.CASCADE, to='PTO_project_core.PTORole', verbose_name='Role'),
        ),
    ]

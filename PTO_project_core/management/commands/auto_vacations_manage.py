# -*- coding: utf-8 -*-
# add this to cron: 0 2 * * * python manage.py auto_vacations_manage
import copy
import datetime
from django.core.management.base import BaseCommand, CommandError
from django.template.loader import render_to_string

from PTO_project_core.helpers.CalendarBuilder import CalendarBuilder
from PTO_project_core.helpers.PTOAsyncMailer import send_mail_async
from PTO_project_core.models import Vacation, VacationStatus, PTOUser


class Command(BaseCommand):
    help = 'Resend email with upcoming vacation, auto-declined started unconfirmed vacations'

    def handle(self, *args, **options):
        now = datetime.datetime.now()
        is_working_day = CalendarBuilder.is_working_day(now.year, now.month, now.day)
        if is_working_day:
            self.__send_upcoming_vacation_request_reminder(now)
            self.__decline_started_new_vacation_request(now)

    def __send_upcoming_vacation_request_reminder(self, now):
        next_working_day = now + datetime.timedelta(days=1)
        while not CalendarBuilder.is_working_day(next_working_day.year, next_working_day.month, next_working_day.day):
            next_working_day = next_working_day + datetime.timedelta(days=1)
        vacations = self.__get_new_vacations_for_day(next_working_day)
        for vacation in vacations:
            user = PTOUser.objects.get(id=vacation.pto_user_id)
            manager = PTOUser.objects.get(id=user.pto_user_manager_id)
            self.__send_manager_email(user, manager, vacation)

    def __decline_started_new_vacation_request(self, now):
        vacations = self.__get_new_vacations_for_day(now)
        for vacation in vacations:
            vacation.status = VacationStatus.objects.get(text='rejected')
            vacation.save()

    @staticmethod
    def __get_new_vacations_for_day(now):
        from_time = datetime.datetime(now.year, now.month, now.day, hour=1)
        to_time = datetime.datetime(now.year, now.month, now.day, hour=23)
        return Vacation.objects.filter(start_date__gte=from_time, start_date__lte=to_time, status__text__iexact='new')

    @staticmethod
    def __send_manager_email(user, manager, vacation):
        total_days = CalendarBuilder.count_working_day_for_range(vacation.start_date, vacation.end_date).days
        email_parameters = {
            'user': user,
            'comment': vacation.comment,
            'start_date': vacation.start_date,
            'end_date': vacation.end_date,
            'total_days': total_days,
            'vacation_type_name': vacation.vacation_type.name
        }
        send_mail_async(
            subject='New vacation request from {} {}'.format(user.first_name, user.last_name),
            body=render_to_string('mail_messages/_manager_notification.txt', email_parameters),
            from_email='TS PTO management tool <noreply@trustsourcing.com>',
            recipient_list=[manager.email],
            html=render_to_string('mail_messages/manager_notification.html', email_parameters)
        )

import copy

from PTO_project_core.helpers.CalendarBuilder import *
from PTO_project_core.helpers.UserPermissions import UserPermissions
from PTO_project_core.helpers.UserPermissions import ROLE_HR_MANAGER, ROLE_MANAGER
from PTO_project_core.models import Vacation, VacationStatus, VacationType


class VacationDataBuilder(object):
    month = None
    year = None
    calendar_object = None

    def __init__(self, request, year=datetime.datetime.now().year, month=datetime.datetime.now().month):
        self.request = request
        self.month = month
        self.year = year
        self.calendar_object = CalendarBuilder(year, month)

    def __get_vacations_per_day(self):
        vacations = self.__get_vacations_for_month(True)
        calendar_list = self.calendar_object.calendar
        vacations_per_day = {}
        for week in calendar_list:
            week_vacations = {}
            for day in week:
                if not day['other_month']:
                    if day['type'] == 'working-day':
                        day_vacations = {}
                        for vacation in vacations:
                            if self.vacation_contains_day(vacation, day['day']):
                                status = self.__get_status(vacation)
                                if vacation.id in week_vacations:
                                    day_vacations[week_vacations[vacation.id]['order']] = {
                                        'name': week_vacations[vacation.id]['name'],
                                        'show_name': False,
                                        'id': vacation.id,
                                        'status': status,
                                        'order': week_vacations[vacation.id]['order'],
                                        'hours': self.__get_hours_for_vacation_day(vacation, day['day'])
                                    }
                                else:
                                    order = self.__get_free_position(day_vacations)
                                    element = {
                                        'name': self.__get_user_name(vacation),
                                        'show_name': True,
                                        'id': vacation.id,
                                        'status': status,
                                        'order': order,
                                        'hours': self.__get_hours_for_vacation_day(vacation, day['day'])
                                    }
                                    day_vacations[order] = element
                                    week_vacations[vacation.id] = element
                        vacations_per_day[day['day']] = {
                            'type': day['type'],
                            'vacations': day_vacations,
                            'day': day['day'],
                            'is_current_day': day['is_current_day'],
                        }
                    elif day['type'] == 'not-working-day':
                        vacations_per_day[day['day']] = {
                            'type': day['type'],
                            'day': day['day'],
                            'is_current_day': day['is_current_day'],
                            'description': day['description'],
                        }

        return vacations_per_day

    def get_data_for_calendar_table(self):
        vacations_table = {}
        vacations = self.__get_vacations_for_month()
        for vacation in vacations:
            vacation.status.text = self.__get_status(vacation)
            user_name = self.__get_user_name(vacation)
            from_date, to_date = self.__get_vacation_edges(vacation)
            for day in range(from_date, to_date + 1):
                if user_name not in vacations_table:
                    vacations_table[user_name] = copy.deepcopy(self.calendar_object.table_calendar)
                if 'vacation' in vacations_table[user_name][day] and vacation.status.text == 'rejected':
                    continue
                vacations_table[user_name][day]['vacation'] = vacation
            if vacation.status.text in ['new', 'scheduled', 'non-paid', 'from-home']:
                last_vacation_day = to_date
                while self.calendar_object.table_calendar[last_vacation_day]['type'] == 'not-working-day' \
                        and last_vacation_day > from_date:
                    last_vacation_day -= 1
                vacations_table[user_name][last_vacation_day]['last'] = True
        return vacations_table

    @staticmethod
    def __get_free_position(day_vacations):
        order = 0
        while 1:
            if order not in day_vacations:
                return order
            order += 1

    def vacation_contains_day(self, vacation, day):
        day_before_start_day = vacation.start_date - datetime.timedelta(days=1)
        if day_before_start_day <= datetime.datetime(self.year, self.month, day) <= vacation.end_date:
            return True
        return False

    def __get_vacations_for_month(self, get_all=False):
        week_day, last_day = monthrange(self.year, self.month)
        end_of_month = datetime.datetime(
            year=self.year, month=self.month, day=last_day, hour=23, minute=59, second=59).strftime("%Y-%m-%d %H:%M:%S")
        start_of_month = datetime.datetime(
            year=self.year, month=self.month, day=1).strftime("%Y-%m-%d %H:%M:%S")

        user_permissions = UserPermissions(self.request)
        if user_permissions.role_id == ROLE_HR_MANAGER or get_all:
            vacations = Vacation.objects.filter(
                start_date__lte=end_of_month, end_date__gte=start_of_month,
                # pto_user__pto_company__id=self.request.session['pto_google_auth']['PTOUser']['pto_company_id'],
                status__in=self.__get_statuses_for_show(),
            ).order_by('start_date')
        elif user_permissions.role_id == ROLE_MANAGER:
            vacations = Vacation.objects.filter(
                start_date__lte=end_of_month, end_date__gte=start_of_month,
                pto_user__pto_user_manager=self.request.session['pto_google_auth']['PTOUser']['id'],
                status__in=self.__get_statuses_for_show(),
            ).order_by('start_date')
        else:
            vacations = None
        return vacations

    @staticmethod
    def __get_user_name(vacation):
        return vacation.pto_user.first_name + ' ' + vacation.pto_user.last_name

    def get_data_for_calendar(self):
        calendar_object = CalendarBuilder(self.year, self.month)
        vacation_per_date = self.__get_vacations_per_day()
        vacation_per_calendar = calendar_object.calendar
        i = 0
        for week in calendar_object.calendar:
            j = 0
            for day in week:
                if not day['other_month']:
                    vacation_per_calendar[i][j] = vacation_per_date[day['day']]
                    vacation_per_calendar[i][j]['other_month'] = False
                else:
                    vacation_per_calendar[i][j] = {
                        'type': day['type'],
                        'day': day['day'],
                        'other_month': day['other_month'],
                    }
                j += 1
            i += 1
        return vacation_per_calendar

    def __get_vacation_edges(self, vacation):
        from_date = vacation.start_date.day
        to_date = vacation.end_date.day
        if vacation.start_date.month < self.month or vacation.start_date.year < self.year:
            from_date = 1
        if vacation.end_date.month > self.month or vacation.end_date.year > self.year:
            to_date = self.calendar_object.table_calendar.__len__()
        return from_date, to_date

    @staticmethod
    def get_vacations_days(user, statuses):
        statuses_ids = VacationStatus.objects.filter(text__in=statuses).values('id')
        included_type_ids = VacationType.objects.filter(use_in_calculations=True).values('id')
        vacations = Vacation.objects.filter(
            pto_user=user.id, status__in=statuses_ids,
            vacation_type__in=included_type_ids,
            end_date__gte=user.get_current_start_of_period_date(),
            start_date__lte=user.get_current_end_of_period_date(),
        )
        return VacationDataBuilder.get_days_hours_for_vacations(vacations)

    @staticmethod
    def get_previous_year_vacations_days(user, statuses):
        statuses_ids = VacationStatus.objects.filter(text__in=statuses).values('id')
        included_type_ids = VacationType.objects.filter(use_in_calculations=True).values('id')
        previous_period_date = user.get_current_start_of_period_date() - datetime.timedelta(days=1)
        vacations = Vacation.objects.filter(
            pto_user=user.id, status__in=statuses_ids,
            vacation_type__in=included_type_ids,
            end_date__gte=user.get_start_of_period_date(previous_period_date),
            start_date__lte=user.get_end_of_period_date(previous_period_date),
        )
        return VacationDataBuilder.get_days_hours_for_vacations(vacations)

    @staticmethod
    def get_vacations_days_with_previous_year(user, vacations, statuses_ids, included_type_ids):
        filtered_vacations = []
        for vacation in vacations:
            if vacation.status_id in statuses_ids and vacation.vacation_type_id in included_type_ids:
                filtered_vacations.append(vacation)
        return VacationDataBuilder.get_days_hours_for_vacations(filtered_vacations)

    @staticmethod
    def get_vacations_days_with_previous_year_before_date(date, user, vacations, statuses_ids, included_type_ids):
        filtered_vacations = []
        for vacation in vacations:
            if vacation.status_id in statuses_ids and vacation.vacation_type_id in included_type_ids \
                    and vacation.end_date < date:
                filtered_vacations.append(vacation)
        return VacationDataBuilder.get_days_hours_for_vacations(filtered_vacations)

    @staticmethod
    def get_current_vacations_days(user, statuses):
        statuses_ids = VacationStatus.objects.filter(text__in=statuses).values('id')
        included_type_ids = VacationType.objects.filter(use_in_calculations=True).values('id')
        vacations = Vacation.objects.filter(
            pto_user=user.id, status__in=statuses_ids,
            vacation_type__in=included_type_ids,
            end_date__gte=user.get_current_start_of_period_date(),
            start_date__lte=datetime.datetime.now(),
        )
        return VacationDataBuilder.get_days_hours_for_vacations(vacations)

    def get_next_prev_availability(self):
        has_prev = True
        has_next = True
        statuses = self.__get_statuses_for_show()

        try:
            max_date = Vacation.objects.filter(
                # pto_user__pto_company__id=self.request.session['pto_google_auth']['PTOUser']['pto_company_id'],
                status__in=statuses
            ).latest('end_date').end_date
        except Vacation.DoesNotExist:
            max_date = datetime.date.today()

        try:
            min_date = Vacation.objects.filter(
                # pto_user__pto_company__id=self.request.session['pto_google_auth']['PTOUser']['pto_company_id'],
                status__in=statuses
            ).earliest('start_date').start_date
        except Vacation.DoesNotExist:
            min_date = datetime.date.today()

        if self.year >= max_date.year and self.month >= max_date.month:
            has_next = False
        if self.year <= min_date.year and self.month <= min_date.month:
            has_prev = False

        return has_prev, has_next

    @staticmethod
    def get_vacations_days_before_date(user, date, statuses_ids, included_type_ids):
        end_date = datetime.datetime(
            year=date.year, month=date.month, day=date.day, hour=23, minute=59, second=59)
        from PTO_project_core.helpers.UsersHelper import HIDDEN_VACATION_NAMES
        vacations = Vacation.objects.filter(
            start_date__lte=end_date,
            vacation_type__in=included_type_ids,
            pto_user=user.id,
            status__in=statuses_ids,
            end_date__gte=user.get_start_of_period_date(end_date),
        ).exclude(vacation_type__name__in=HIDDEN_VACATION_NAMES,
                  status__text=['rejected'])
        return VacationDataBuilder.get_days_hours_for_vacations(vacations)

    @staticmethod
    def __get_statuses_for_show():
        statuses = ['new', 'scheduled', 'rejected']
        statuses_ids = VacationStatus.objects.filter(text__in=statuses).values('id')
        return statuses_ids

    @staticmethod
    def get_days_hours_for_vacations(vacations):
        days_hours_total = DateHour()
        for vacation in vacations:
            days_hours = CalendarBuilder.count_working_day_for_range(vacation.start_date, vacation.end_date)
            days_hours_total.add_days_hours(days_hours)
        return days_hours_total

    def __get_hours_for_vacation_day(self, vacation, day):
        days_hours = DateHour(0, 0)
        start_date_hour = float(vacation.start_date.hour)
        if vacation.start_date.minute > 0:
            start_date_hour += float(vacation.start_date.minute) / 60
        end_date_hour = float(vacation.end_date.hour)
        if vacation.end_date.minute > 0:
            end_date_hour += float(vacation.end_date.minute) / 60

        if vacation.start_date.year == vacation.end_date.year and \
           vacation.start_date.month == vacation.end_date.month and \
           vacation.start_date.day == vacation.end_date.day:
            days_hours.add_days_hours(DateHour(0, CalendarBuilder.get_hours_interval(start_date_hour, end_date_hour)))
        else:
            if vacation.start_date.year == self.year and \
                            vacation.start_date.month == self.month and \
                            vacation.start_date.day == day:
                days_hours.add_days_hours(
                    DateHour(0, CalendarBuilder.get_hours_interval(start_date_hour, END_WORKING_HOUR)))

            if vacation.end_date.year == self.year and \
                            vacation.end_date.month == self.month and \
                            vacation.end_date.day == day:
                hours = CalendarBuilder.get_hours_interval(START_WORKING_HOUR, end_date_hour)
                days_hours.add_days_hours(
                    DateHour(0, CalendarBuilder.get_hours_interval(START_WORKING_HOUR, end_date_hour)))
        return days_hours

    @staticmethod
    def get_static_vacations(user, vacations, statuses_ids, vacations_type_ids):
        filtered_vacations = []
        for vacation in vacations:
            if vacation.status_id in statuses_ids and vacation.vacation_type_id in vacations_type_ids:
                filtered_vacations.append(vacation)
        return VacationDataBuilder.get_days_hours_for_vacations(filtered_vacations)

    @staticmethod
    def __get_status(vacation):
        from PTO_project_core.helpers.UsersHelper import NON_PAID_VACATION_NAME
        from PTO_project_core.helpers.UsersHelper import SICK_LEAVE_VACATION_NAME
        from PTO_project_core.helpers.UsersHelper import WORK_FROM_HOME_VACATION_NAME
        if vacation.vacation_type.name == NON_PAID_VACATION_NAME \
                and vacation.status.text == 'scheduled':
            return 'non-paid'
        elif vacation.vacation_type.name == SICK_LEAVE_VACATION_NAME \
                and vacation.status.text == 'scheduled':
            return 'sick'
        elif vacation.vacation_type.name == WORK_FROM_HOME_VACATION_NAME \
                and vacation.status.text == 'scheduled':
            return 'from-home'
        else:
            return vacation.status.text

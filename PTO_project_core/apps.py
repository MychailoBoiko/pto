from __future__ import unicode_literals

from django.apps import AppConfig


class PtoProjectCoreConfig(AppConfig):
    name = 'PTO_project_core'
    verbose_name = 'PTO project'

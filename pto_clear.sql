/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50509
Source Host           : localhost:3306
Source Database       : pto

Target Server Type    : MYSQL
Target Server Version : 50509
File Encoding         : 65001

Date: 2016-02-25 18:57:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_0e939a4f` (`group_id`),
  KEY `auth_group_permissions_8373b171` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_group_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  KEY `auth_permission_417f1b1c` (`content_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES ('1', 'Can add log entry', '1', 'add_logentry');
INSERT INTO `auth_permission` VALUES ('2', 'Can change log entry', '1', 'change_logentry');
INSERT INTO `auth_permission` VALUES ('3', 'Can delete log entry', '1', 'delete_logentry');
INSERT INTO `auth_permission` VALUES ('4', 'Can add permission', '2', 'add_permission');
INSERT INTO `auth_permission` VALUES ('5', 'Can change permission', '2', 'change_permission');
INSERT INTO `auth_permission` VALUES ('6', 'Can delete permission', '2', 'delete_permission');
INSERT INTO `auth_permission` VALUES ('7', 'Can add group', '3', 'add_group');
INSERT INTO `auth_permission` VALUES ('8', 'Can change group', '3', 'change_group');
INSERT INTO `auth_permission` VALUES ('9', 'Can delete group', '3', 'delete_group');
INSERT INTO `auth_permission` VALUES ('10', 'Can add user', '4', 'add_user');
INSERT INTO `auth_permission` VALUES ('11', 'Can change user', '4', 'change_user');
INSERT INTO `auth_permission` VALUES ('12', 'Can delete user', '4', 'delete_user');
INSERT INTO `auth_permission` VALUES ('13', 'Can add content type', '5', 'add_contenttype');
INSERT INTO `auth_permission` VALUES ('14', 'Can change content type', '5', 'change_contenttype');
INSERT INTO `auth_permission` VALUES ('15', 'Can delete content type', '5', 'delete_contenttype');
INSERT INTO `auth_permission` VALUES ('16', 'Can add session', '6', 'add_session');
INSERT INTO `auth_permission` VALUES ('17', 'Can change session', '6', 'change_session');
INSERT INTO `auth_permission` VALUES ('18', 'Can delete session', '6', 'delete_session');
INSERT INTO `auth_permission` VALUES ('19', 'Can add pto role', '7', 'add_ptorole');
INSERT INTO `auth_permission` VALUES ('20', 'Can change pto role', '7', 'change_ptorole');
INSERT INTO `auth_permission` VALUES ('21', 'Can delete pto role', '7', 'delete_ptorole');
INSERT INTO `auth_permission` VALUES ('22', 'Can add pto user', '8', 'add_ptouser');
INSERT INTO `auth_permission` VALUES ('23', 'Can change pto user', '8', 'change_ptouser');
INSERT INTO `auth_permission` VALUES ('24', 'Can delete pto user', '8', 'delete_ptouser');
INSERT INTO `auth_permission` VALUES ('25', 'Can add vacation type', '9', 'add_vacationtype');
INSERT INTO `auth_permission` VALUES ('26', 'Can change vacation type', '9', 'change_vacationtype');
INSERT INTO `auth_permission` VALUES ('27', 'Can delete vacation type', '9', 'delete_vacationtype');
INSERT INTO `auth_permission` VALUES ('28', 'Can add vacation status', '10', 'add_vacationstatus');
INSERT INTO `auth_permission` VALUES ('29', 'Can change vacation status', '10', 'change_vacationstatus');
INSERT INTO `auth_permission` VALUES ('30', 'Can delete vacation status', '10', 'delete_vacationstatus');
INSERT INTO `auth_permission` VALUES ('31', 'Can add vacation', '11', 'add_vacation');
INSERT INTO `auth_permission` VALUES ('32', 'Can change vacation', '11', 'change_vacation');
INSERT INTO `auth_permission` VALUES ('33', 'Can delete vacation', '11', 'delete_vacation');

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user
-- ----------------------------

-- ----------------------------
-- Table structure for auth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_e8701ad4` (`user_id`),
  KEY `auth_user_groups_0e939a4f` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user_groups
-- ----------------------------

-- ----------------------------
-- Table structure for auth_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_e8701ad4` (`user_id`),
  KEY `auth_user_user_permissions_8373b171` (`permission_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of auth_user_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_417f1b1c` (`content_type_id`),
  KEY `django_admin_log_e8701ad4` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO `django_content_type` VALUES ('1', 'admin', 'logentry');
INSERT INTO `django_content_type` VALUES ('2', 'auth', 'permission');
INSERT INTO `django_content_type` VALUES ('3', 'auth', 'group');
INSERT INTO `django_content_type` VALUES ('4', 'auth', 'user');
INSERT INTO `django_content_type` VALUES ('5', 'contenttypes', 'contenttype');
INSERT INTO `django_content_type` VALUES ('6', 'sessions', 'session');
INSERT INTO `django_content_type` VALUES ('7', 'PTO_project_core', 'ptorole');
INSERT INTO `django_content_type` VALUES ('8', 'PTO_project_core', 'ptouser');
INSERT INTO `django_content_type` VALUES ('9', 'PTO_project_core', 'vacationtype');
INSERT INTO `django_content_type` VALUES ('10', 'PTO_project_core', 'vacationstatus');
INSERT INTO `django_content_type` VALUES ('11', 'PTO_project_core', 'vacation');

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO `django_migrations` VALUES ('1', 'contenttypes', '0001_initial', '2016-02-25 15:48:07');
INSERT INTO `django_migrations` VALUES ('2', 'auth', '0001_initial', '2016-02-25 15:48:08');
INSERT INTO `django_migrations` VALUES ('3', 'admin', '0001_initial', '2016-02-25 15:48:08');
INSERT INTO `django_migrations` VALUES ('4', 'admin', '0002_logentry_remove_auto_add', '2016-02-25 15:48:08');
INSERT INTO `django_migrations` VALUES ('5', 'contenttypes', '0002_remove_content_type_name', '2016-02-25 15:48:08');
INSERT INTO `django_migrations` VALUES ('6', 'auth', '0002_alter_permission_name_max_length', '2016-02-25 15:48:09');
INSERT INTO `django_migrations` VALUES ('7', 'auth', '0003_alter_user_email_max_length', '2016-02-25 15:48:09');
INSERT INTO `django_migrations` VALUES ('8', 'auth', '0004_alter_user_username_opts', '2016-02-25 15:48:09');
INSERT INTO `django_migrations` VALUES ('9', 'auth', '0005_alter_user_last_login_null', '2016-02-25 15:48:09');
INSERT INTO `django_migrations` VALUES ('10', 'auth', '0006_require_contenttypes_0002', '2016-02-25 15:48:09');
INSERT INTO `django_migrations` VALUES ('11', 'auth', '0007_alter_validators_add_error_messages', '2016-02-25 15:48:09');
INSERT INTO `django_migrations` VALUES ('12', 'sessions', '0001_initial', '2016-02-25 15:48:09');
INSERT INTO `django_migrations` VALUES ('13', 'PTO_project_core', '0001_initial', '2016-02-25 15:52:43');
INSERT INTO `django_migrations` VALUES ('14', 'PTO_project_core', '0002_ptouser_pto_role', '2016-02-25 16:06:19');

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of django_session
-- ----------------------------

-- ----------------------------
-- Table structure for pto_project_core_ptorole
-- ----------------------------
DROP TABLE IF EXISTS `pto_project_core_ptorole`;
CREATE TABLE `pto_project_core_ptorole` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(31) NOT NULL,
  `create_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pto_project_core_ptorole
-- ----------------------------
INSERT INTO `pto_project_core_ptorole` VALUES ('1', 'manager', '0000-00-00 00:00:00');
INSERT INTO `pto_project_core_ptorole` VALUES ('2', 'user', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for pto_project_core_ptouser
-- ----------------------------
DROP TABLE IF EXISTS `pto_project_core_ptouser`;
CREATE TABLE `pto_project_core_ptouser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(63) NOT NULL,
  `middle_name` varchar(63) DEFAULT NULL,
  `last_name` varchar(63) NOT NULL,
  `email` varchar(254) NOT NULL,
  `position` varchar(63) NOT NULL,
  `available_vacation_days` int(11) NOT NULL,
  `create_datetime` datetime NOT NULL,
  `employment_start_datetime` datetime DEFAULT NULL,
  `pto_role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `PTO_project_core_ptouser_f8b24060` (`pto_role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pto_project_core_ptouser
-- ----------------------------
INSERT INTO `pto_project_core_ptouser` VALUES ('1', 'Misha', '', 'Boiko', 'misha.boiko@gmail.com', 'devepoper', '200', '0000-00-00 00:00:00', '2013-10-16 17:55:51', '2');

-- ----------------------------
-- Table structure for pto_project_core_vacation
-- ----------------------------
DROP TABLE IF EXISTS `pto_project_core_vacation`;
CREATE TABLE `pto_project_core_vacation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `create_datetime` datetime NOT NULL,
  `pto_user_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `vacation_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `PTO_project_core_vacation_70fd32e0` (`pto_user_id`),
  KEY `PTO_project_core_vacation_dc91ed4b` (`status_id`),
  KEY `PTO_project_core_vacation_80a42a98` (`vacation_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pto_project_core_vacation
-- ----------------------------

-- ----------------------------
-- Table structure for pto_project_core_vacationstatus
-- ----------------------------
DROP TABLE IF EXISTS `pto_project_core_vacationstatus`;
CREATE TABLE `pto_project_core_vacationstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(31) NOT NULL,
  `create_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pto_project_core_vacationstatus
-- ----------------------------
INSERT INTO `pto_project_core_vacationstatus` VALUES ('1', 'new', '0000-00-00 00:00:00');
INSERT INTO `pto_project_core_vacationstatus` VALUES ('2', 'scheduled', '0000-00-00 00:00:00');
INSERT INTO `pto_project_core_vacationstatus` VALUES ('3', 'viewed', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for pto_project_core_vacationtype
-- ----------------------------
DROP TABLE IF EXISTS `pto_project_core_vacationtype`;
CREATE TABLE `pto_project_core_vacationtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(31) NOT NULL,
  `create_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pto_project_core_vacationtype
-- ----------------------------
INSERT INTO `pto_project_core_vacationtype` VALUES ('1', 'Annual leave', '0000-00-00 00:00:00');
INSERT INTO `pto_project_core_vacationtype` VALUES ('2', 'Non-paid leave', '0000-00-00 00:00:00');
INSERT INTO `pto_project_core_vacationtype` VALUES ('3', 'Sick leave', '0000-00-00 00:00:00');
INSERT INTO `pto_project_core_vacationtype` VALUES ('4', 'Maternity leave', '0000-00-00 00:00:00');
INSERT INTO `pto_project_core_vacationtype` VALUES ('5', 'Peternity leave', '0000-00-00 00:00:00');
INSERT INTO `pto_project_core_vacationtype` VALUES ('6', 'Adoptive leave', '0000-00-00 00:00:00');
INSERT INTO `pto_project_core_vacationtype` VALUES ('7', 'Carer\'s leave', '0000-00-00 00:00:00');
INSERT INTO `pto_project_core_vacationtype` VALUES ('8', 'Parental leave', '0000-00-00 00:00:00');
INSERT INTO `pto_project_core_vacationtype` VALUES ('9', 'Force majeure leave', '0000-00-00 00:00:00');
INSERT INTO `pto_project_core_vacationtype` VALUES ('10', 'Compassionate leave', '0000-00-00 00:00:00');
INSERT INTO `pto_project_core_vacationtype` VALUES ('11', 'Jury service', '0000-00-00 00:00:00');
INSERT INTO `pto_project_core_vacationtype` VALUES ('12', 'Career break or study leave', '0000-00-00 00:00:00');

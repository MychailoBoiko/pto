/**
 * PTO - on 3/09/2016.
 */


module.exports = function (grunt) {
    grunt.initConfig({
        sass: {
            dist: {
                files: {
                    'PTO_project/static/css/style.css': 'PTO_project/static/sass/style.scss'
                }
            }
        },
        concat_css: {
            all: {
                src: ['PTO_project/static/css/vendor/*.css', 'PTO_project/static/css/style.css'],
                dest: 'PTO_project/static/css/style.css'
            },
        },
        //cssmin: {
        //    dist: {
        //        options: {
        //            keepSpecialComments: 0
        //        },
        //        files: {
        //            'Dist/styles/style.min.css': ['Dist/styles/style.css']
        //        }
        //    }
        //},
        //
        watch: {
            styles: {
                files: ['PTO_project/static/sass/**/*.scss'],
                tasks: ['sass', 'concat_css'],
                options: {
                    spawn: false
                }
            }
        }
    });


    // grunt.loadNpmTasks("grunt-contrib-sass");
    grunt.loadNpmTasks("grunt-sass");
    grunt.loadNpmTasks("grunt-contrib-watch");
    //grunt.loadNpmTasks("grunt-contrib-concat");
    //grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    //
    grunt.registerTask("default", ["watch"]);

    // grunt.registerTask("all", ["sass", "concat_css", "concat"]);
    grunt.registerTask("all", ["sass", "concat_css"]);

    //grunt.registerTask("dist", ["uglify", "cssmin"]);


}